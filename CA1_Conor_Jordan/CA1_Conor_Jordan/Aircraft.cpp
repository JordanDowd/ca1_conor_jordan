#include "Aircraft.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>
#include <iostream>

using namespace std::placeholders;

namespace
{
	const std::vector<AircraftData> Table = initializeAircraftData();
}

Aircraft::Aircraft(Type type, const TextureHolder& textures, const FontHolder& fonts)
	: Entity(Table[type].hitpoints)
	, mType(type)
	, mSprite(textures.get(Table[type].texture), Table[type].textureRect)
	, mExplosion(textures.get(Textures::Explosion))
	, mFireCommand()
	, mMissileCommand()
	, mCaughtCommand()
	, mFireCountdown(sf::Time::Zero)
	, mIsFiring(false)
	, mBallCanMove(false)
	, mBallCollided(false)
	, mIsLaunchingMissile(false)
	, mShowExplosion(true)
	, mExplosionBegan(false)
	, mSpawnedPickup(false)
	, mPickupsEnabled(true)
	, mFireRateLevel(1)
	, mSpreadLevel(1)
	, mMissileAmmo(0)
	, mDropPickupCommand()
	, mTravelledDistance(0.f)
	, mDirectionIndex(0)
	, mMissileDisplay(nullptr)
	, mIdentifier(0)
	, mBallCaught(false)
	, mIntercepted(false)
{
	mExplosion.setFrameSize(sf::Vector2i(256, 256));
	mExplosion.setNumFrames(16);
	mExplosion.setDuration(sf::seconds(1));

	centerOrigin(mSprite);
	//centerOrigin(mExplosion);

	mFireCommand.category = Category::SceneAirLayer;
	mFireCommand.action   = [this, &textures] (SceneNode& node, sf::Time)
	{
		createBullets(node, textures);
	};

	mMissileCommand.category = Category::SceneAirLayer;
	mMissileCommand.action   = [this, &textures] (SceneNode& node, sf::Time)
	{
		createProjectile(node, 0.f, 0.5f, textures);
	};

	mDropPickupCommand.category = Category::SceneAirLayer;
	mDropPickupCommand.action   = [this, &textures] (SceneNode& node, sf::Time)
	{
		createPickup(node, textures);
	};

	mCaughtCommand.category = Category::SceneAirLayer;
	mCaughtCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		dealWithBall();
	};

	std::unique_ptr<TextNode> healthDisplay(new TextNode(fonts, ""));
	mHealthDisplay = healthDisplay.get();
	attachChild(std::move(healthDisplay));

	if (getCategory() == Category::PlayerAircraft)
	{
		std::unique_ptr<TextNode> missileDisplay(new TextNode(fonts, ""));
		missileDisplay->setPosition(0, 70);
		mMissileDisplay = missileDisplay.get();
		attachChild(std::move(missileDisplay));
	}
	else
	{
		setVelocity(200, 200);
	}
	updateTexts();
}

void Aircraft::dealWithBall()
{
	mBall->setPlayerCaught(this, true);
}

void Aircraft::setIsFiring(bool firing)
{
	mIsFiring = firing;
	//mFireCountdown += Table[mType].fireInterval / (mFireRateLevel + 1.f);
}

void Aircraft::setBallCanMove(bool move)
{
	mBallCanMove = move;
}

void Aircraft::setIntercepted(bool intercepted)
{
	mIntercepted = intercepted;
}

void Aircraft::setIsBallCaught(Projectile* ball, bool caught)
{
	mBallCaught = caught;
	mBall = ball;
}

void Aircraft::setIsBallCaught(bool caught)
{
	mBallCaught = caught;
}

Projectile * Aircraft::getBall()
{
	return mBall;
}

bool Aircraft::isBallCaught()
{
	return mBallCaught;
}

void Aircraft::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (isDestroyed() && mShowExplosion)
		target.draw(mExplosion, states);
	else
		target.draw(mSprite, states);
}

void Aircraft::disablePickups()
{
	mPickupsEnabled = false;
}

void Aircraft::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	// Update texts and roll animation
	//updateTexts();
	updateRollAnimation();

	// Entity has been destroyed: Possibly drop pickup, mark for removal
	if (isDestroyed())
	{
		checkPickupDrop(commands);
		mExplosion.update(dt);

		// Play explosion sound only once
		if (!mExplosionBegan)
		{
			// Play sound effect
			SoundEffect::ID soundEffect = (randomInt(2) == 0) ? SoundEffect::Explosion1 : SoundEffect::Explosion2;
			playLocalSound(commands, soundEffect);

			// Emit network game action for enemy explosions
			if (!isAllied())
			{
				sf::Vector2f position = getWorldPosition();

				Command command;
				command.category = Category::Network;
				command.action = derivedAction<NetworkNode>([position] (NetworkNode& node, sf::Time)
				{
					node.notifyGameAction(GameActions::EnemyExplode, position);
				});

				commands.push(command);
			}

			mExplosionBegan = true;
		}
		return;
	}

	// Check if bullets or missiles are fired
	checkProjectileLaunch(dt, commands);

	// Update enemy movement pattern; apply velocity
	updateMovementPattern(dt);
	Entity::updateCurrent(dt, commands);
}

unsigned int Aircraft::getCategory() const
{
	if (isAllied())
		return Category::PlayerAircraft;
	else
		return Category::EnemyAircraft;
}

sf::FloatRect Aircraft::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

bool Aircraft::isMarkedForRemoval() const
{
	return isDestroyed() && (mExplosion.isFinished() || !mShowExplosion);
}

void Aircraft::remove()
{
	Entity::remove();
	mShowExplosion = false;
}

bool Aircraft::isAllied() const
{
	return mType == Eagle || mType == HorizontalWall || mType == Player2;
}

float Aircraft::getMaxSpeed() const
{
	return Table[mType].speed;
}

void Aircraft::fire()
{
	// Only ships with fire interval != 0 are able to fire
	if (mFireCountdown <= sf::Time::Zero)
		mIsFiring = true;
}

void Aircraft::ballCollidedWithPlayer()
{
	mBallCollided = true;
}

void Aircraft::launchMissile()
{
	if (mMissileAmmo > 0)
	{
		mIsLaunchingMissile = true;
		--mMissileAmmo;
	}
}

void Aircraft::playLocalSound(CommandQueue& commands, SoundEffect::ID effect)
{
	sf::Vector2f worldPosition = getWorldPosition();

	Command command;
	command.category = Category::SoundEffect;
	command.action = derivedAction<SoundNode>(
		[effect, worldPosition] (SoundNode& node, sf::Time)
		{
			node.playSound(effect, worldPosition); 
		});

	commands.push(command);
}

int	Aircraft::getIdentifier()
{
	return mIdentifier;
}

void Aircraft::setIdentifier(int identifier)
{
	mIdentifier = identifier;
}

void Aircraft::updateMovementPattern(sf::Time dt)
{
	//// Enemy airplane: Movement pattern
	//const std::vector<Direction>& directions = Table[mType].directions;
	//if (!directions.empty())
	//{
	//	// Moved long enough in current direction: Change direction
	//	if (mTravelledDistance > directions[mDirectionIndex].distance)
	//	{
	//		mDirectionIndex = (mDirectionIndex + 1) % directions.size();
	//		mTravelledDistance = 0.f;
	//	}

	//	// Compute velocity from direction
	//	float radians = toRadian(directions[mDirectionIndex].angle + 90.f);
	//	float vx = getMaxSpeed() * std::cos(radians);
	//	float vy = getMaxSpeed() * std::sin(radians);

	//	setVelocity(vx, vy);

	//	mTravelledDistance += getMaxSpeed() * dt.asSeconds();
	//}
	if (!isAllied())
	{
		setVelocity(getVelocity().x, getVelocity().y);
		
	}
	if (isBallCaught())
	{
		setVelocity(getVelocity() * 0.9f);
	}
}

void Aircraft::checkPickupDrop(CommandQueue& commands)
{
	// Drop pickup, if enemy airplane, with probability 1/3, if pickup not yet dropped
	// and if not in network mode (where pickups are dropped via packets)
	if (!isAllied() && randomInt(3) == 0 && !mSpawnedPickup && mPickupsEnabled)
		commands.push(mDropPickupCommand);

	mSpawnedPickup = true;
}

sf::Vector2f Aircraft::getCenterPosition()
{
	sf::Vector2f centerPos = (getWorldPosition());
	return centerPos;
}

void Aircraft::checkProjectileLaunch(sf::Time dt, CommandQueue& commands)
{
	
	// Check for automatic gunfire, allow only in intervals
	if (mIsFiring && mFireCountdown <= sf::Time::Zero)
	{
		//std::cout << "First IF: " << std::endl;
		// Interval expired: We can fire a new bullet
		commands.push(mFireCommand);
		playLocalSound(commands, wasIntercepted() ? SoundEffect::InterceptBall : SoundEffect::FireBall);
		mIsFiring = false;
		mIntercepted = false;
		mBallCanMove = true;
		mFireCountdown += Table[mType].fireInterval / (mFireRateLevel + 1.f);
	}
	else if (mFireCountdown > sf::Time::Zero)
	{
		
		
		// Interval not expired: Decrease it further
		mFireCountdown -= dt;
		//mIsFiring = false;
	}
	else
	{
		mBallCanMove = false;
	}

	// Check for missile launch
	if (mIsLaunchingMissile)
	{
		commands.push(mMissileCommand);
		playLocalSound(commands, SoundEffect::LaunchMissile);

		mIsLaunchingMissile = false;
	}
	
	if (mBallCollided)
	{
		mBallCollided = false;
		commands.push(mCaughtCommand);
	}
}

void Aircraft::createBullets(SceneNode& node, const TextureHolder& textures) const
{
	switch (mSpreadLevel)
	{
		case 1:
			createProjectile(node,0.0f, 0.0f, textures);
			break;

		case 2:
			createProjectile(node, -0.33f, 0.33f, textures);
			createProjectile(node, +0.33f, 0.33f, textures);
			break;

		case 3:
			createProjectile(node,-0.5f, 0.33f, textures);
			createProjectile(node,  0.0f, 0.5f, textures);
			createProjectile(node,+0.5f, 0.33f, textures);
			break;
	}
}

void Aircraft::createProjectile(SceneNode& node, float xOffset, float yOffset, const TextureHolder& textures) const
{
	sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
	float sign = isAllied() ? -1.f : +1.f;
	//projectile->setPosition(getWorldPosition() + offset * sign);
	//projectile->setVelocity(getVelocity().x *3, getVelocity().y *3);
	//node.attachChild(std::move(projectile));
	if (mBallCaught)
	{
		mBall->setPosition(getWorldPosition() + offset * sign);
		mBall->setVelocity(getVelocity().x * 5, getVelocity().y * 5);
		mBall->setPlayerCaught(false);
	}
	
}

void Aircraft::createPickup(SceneNode& node, const TextureHolder& textures) const
{
	auto type = static_cast<Pickup::Type>(randomInt(Pickup::TypeCount));

	std::unique_ptr<Pickup> pickup(new Pickup(type, textures));
	pickup->setPosition(getWorldPosition());
	pickup->setVelocity(0.f, 1.f);
	node.attachChild(std::move(pickup));
}

void Aircraft::updateTexts()
{
	// Display hitpoints
	if (isDestroyed())
		mHealthDisplay->setString("");
	else
		mHealthDisplay->setString(toString(""));
	mHealthDisplay->setPosition(0.f, 50.f);
	mHealthDisplay->setRotation(-getRotation());

	// Display missiles, if available
	if (mMissileDisplay)
	{
		if (mMissileAmmo == 0 || isDestroyed())
			mMissileDisplay->setString("");
		else
			mMissileDisplay->setString("M: " + toString(mMissileAmmo));
	}
}

void Aircraft::updateRollAnimation()
{
	if (Table[mType].hasRollAnimation)
	{
		sf::IntRect textureRect = Table[mType].textureRect;

		// Roll left: Texture rect offset once
		if (getVelocity().x < 0.f)
			textureRect.left += textureRect.width;

		// Roll right: Texture rect offset twice
		else if (getVelocity().x > 0.f)
			textureRect.left += 2 * textureRect.width;

		else
			textureRect.left = 0;

		mSprite.setTextureRect(textureRect);
	}
}

void Aircraft::updateTexture()
{
	sf::IntRect textureRect = Table[mType].textureRect;
	textureRect.top += textureRect.height;
	mSprite.setTextureRect(textureRect);
}

bool Aircraft::isFiring()
{
	return mIsFiring;
}

bool Aircraft::ballCanMove()
{
	return mBallCanMove;
}

bool Aircraft::ballCollided()
{
	return mBallCollided;
}

bool Aircraft::wasIntercepted()
{
	return mIntercepted;
}
