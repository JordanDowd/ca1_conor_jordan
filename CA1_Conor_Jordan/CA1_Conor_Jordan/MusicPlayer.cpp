#include "MusicPlayer.hpp"


MusicPlayer::MusicPlayer()
: mMusic()
, mFilenames()
, mVolume(25.f)
{
	mFilenames[Music::MenuTheme]    = "Media/Music/ThemeOne.ogg";
	mFilenames[Music::MissionTheme] = "Media/Music/MenuTheme.ogg";
	mFilenames[Music::ProgressSound] = "Media/Music/progressBar.ogg";
}

void MusicPlayer::play(Music::ID theme)
{
	std::string filename = mFilenames[theme];

	if (!mMusic.openFromFile(filename))
		throw std::runtime_error("Music " + filename + " could not be loaded.");
	mMusic.setVolume(mVolume);
	mMusic.setLoop(true);
	mMusic.play(); 
}

void MusicPlayer::stop()
{
	mMusic.stop();
}

void MusicPlayer::setVolume(float volume)
{
	mVolume = volume;
}

sf::Music& MusicPlayer::getCurrentMusic()
{
	return mMusic;
}

void MusicPlayer::setPaused(bool paused)
{
	if (paused)
		mMusic.pause();
	else
		mMusic.play();
}
