#ifndef BOOK_MULTIPLAYERGAMESTATE_HPP
#define BOOK_MULTIPLAYERGAMESTATE_HPP

#include "State.hpp"
#include "World.hpp"
#include "Player.hpp"
#include "GameServer.hpp"
#include "NetworkProtocol.hpp"

#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <SFML/Network/Packet.hpp>

#include <SFML/Graphics/RectangleShape.hpp>

class SoundPlayer;

class MultiplayerGameState : public State
{
	public:
									MultiplayerGameState(StateStack& stack, Context context, bool isHost);

		virtual void				draw();
		virtual bool				update(sf::Time dt);
		virtual bool				handleEvent(const sf::Event& event);
		virtual void				onActivate();
		void						onDestroy();

		void						disableAllRealtimeActions();


	private:
		void						updateBroadcastMessage(sf::Time elapsedTime);
		void						handlePacket(sf::Int32 packetType, sf::Packet& packet);


	private:
		typedef std::unique_ptr<Player> PlayerPtr;


	private:
		World						mWorld;
		sf::RenderWindow&			mWindow;
		TextureHolder&				mTextureHolder;

		std::map<int, PlayerPtr>	mPlayers;
		std::vector<sf::Int32>		mLocalPlayerIdentifiers;
		std::vector<sf::Int32>		mLocalBallIdentifiers;
		sf::TcpSocket				mSocket;
		bool						mConnected;
		std::unique_ptr<GameServer> mGameServer;
		sf::Clock					mTickClock;

		std::vector<std::string>	mBroadcasts;
		sf::Text					mBroadcastText;
		sf::Time					mBroadcastElapsedTime;

		sf::Text					mPlayerInvitationText;
		sf::Time					mPlayerInvitationTime;

		sf::Text					mLobbyText;
		sf::Time					mLobbyTime;

		sf::Text					mHostText;
		sf::Text					mHostStartText;
		sf::Text					mPlayersConnectedText;
		sf::Text					mCountdownText;

		sf::Text					mGreenProgressValueText;
		int							mGreenBarValue;

		sf::Text					mRedProgressValueText;
		int							mRedBarValue;

		int						    mCountdownTimer;
		sf::Clock					mCountdownClock;

		sf::Text					mFailedConnectionText;
		sf::Clock					mFailedConnectionClock;
		
		sf::RectangleShape			mLobbyScreen;
		sf::RectangleShape			mCountdownScreen;
		sf::Color					mCountdownScreenAlpha;

		SoundPlayer&				mSounds;
		MusicPlayer&				mMusic;
		bool						mActiveState;
		bool						mHasFocus;
		bool						mHost;
		bool						mGameStarted;
		bool						mBeginCountdown;
		bool						mBallSpawned;
		sf::Time					mClientTimeout;
		sf::Time					mTimeSinceLastPacket;

		float						mAlliedProgressBarValue;
		float						mEnemyProgressBarValue;
};

#endif // BOOK_MULTIPLAYERGAMESTATE_HPP
