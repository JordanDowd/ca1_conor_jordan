#ifndef BOOK_WALL_HPP
#define BOOK_WALL_HPP

#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Aircraft.hpp"
#include <SFML/Graphics/Sprite.hpp>


class Aircraft;

class Wall : public Entity
{
public:
	enum Type
	{
		HorizontalWall,
		VerticalWall,
		AlliedObjective,
		AlliedObjectiveProgress,
		EnemyObjective,
		EnemyObjectiveProgress,
		TypeCount
	};


public:
	Wall(Type type, const TextureHolder& textures);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;

	virtual unsigned int	getType() const;
	
	void					updateTexture(float val);
	int getIdentifier();
	void setIdentifier(int identifier);

protected:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void updateCurrent(sf::Time dt, CommandQueue & commands);
	
	

private:
	Type 					mType;
	sf::Sprite				mSprite;
	sf::Vector2f			mTargetDirection;
	float					mValue;
	int						mIdentifier;
	
};

#endif // BOOK_WALL_HPP