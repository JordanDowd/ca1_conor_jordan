#include "Controller.hpp"

#include <SFML/Window/Joystick.hpp>

Controller::Controller(int controllerIndex)
	: mControllerIndex(controllerIndex), mTriggerThreshold(70.f), mThumbstickThreshold(50.f)
{
}

bool Controller::IsConnected()
{
	if (mControllerIndex < 0 || mControllerIndex > sf::Joystick::Count)
	{
		return false;
	}
	else
	{
		return sf::Joystick::isConnected(mControllerIndex);
	}
}

bool Controller::A()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 0);
}

bool Controller::B()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 1);
}

bool Controller::X()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 2);
}

bool Controller::Y()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 3);
}

bool Controller::LB()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 4);
}

bool Controller::RB()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 5);
}

bool Controller::Back()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 6);
}

bool Controller::Start()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 7);
}

bool Controller::L3()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 8);
}

bool Controller::R3()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 9);
}

// Button id doesn't work, may be reserved for something else
bool Controller::XboxButton()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 10);
}

bool Controller::LT()
{
	return TValue() > mTriggerThreshold;
}

bool Controller::RT()
{
	return TValue() < -mTriggerThreshold;
}

bool Controller::DpadUp()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 11);
}

bool Controller::DpadDown()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 12);
}

bool Controller::DpadLeft()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 13);
}

bool Controller::DpadRight()
{
	return sf::Joystick::isButtonPressed(mControllerIndex, 14);
}

sf::Vector2f Controller::LeftThumbstick()
{
	return sf::Vector2f(sf::Joystick::getAxisPosition(mControllerIndex, sf::Joystick::X),
		sf::Joystick::getAxisPosition(mControllerIndex, sf::Joystick::Y));
}

sf::Vector2f Controller::RightThumbstick()
{
	return sf::Vector2f(sf::Joystick::getAxisPosition(mControllerIndex, sf::Joystick::U),
		sf::Joystick::getAxisPosition(mControllerIndex, sf::Joystick::V));
}

float Controller::TValue()
{
	//<summary>Positive = Left Trigger. Negative = Right Trigger</summary>
	return sf::Joystick::getAxisPosition(mControllerIndex, sf::Joystick::Z);
}

// ------------------ MUTATORS ----------------------------

int Controller::GetControllerIndex() const
{
	return mControllerIndex;
}

float Controller::GetTriggerThreshold() const
{
	return mTriggerThreshold;
}

float Controller::GetThumbstickThreshold() const
{
	return mThumbstickThreshold;
}

void Controller::SetTriggerThreshold(float threshold)
{
	mTriggerThreshold = threshold;
}

void Controller::SetThumbstickThreshold(float threshold)
{
	mThumbstickThreshold = threshold;
}