#include "DataTables.hpp"
#include "Aircraft.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "Particle.hpp"
#include "Wall.hpp"


// For std::bind() placeholders _1, _2, ...
using namespace std::placeholders;

std::vector<AircraftData> initializeAircraftData()
{
	std::vector<AircraftData> data(Aircraft::TypeCount);

	data[Aircraft::Eagle].hitpoints = 100;
	data[Aircraft::Eagle].speed = 200.f;
	data[Aircraft::Eagle].fireInterval = sf::seconds(1.5f);
	data[Aircraft::Eagle].texture = Textures::Entities;
	data[Aircraft::Eagle].textureRect = sf::IntRect(32, 0, 32, 32);
	data[Aircraft::Eagle].hasRollAnimation = false;

	data[Aircraft::Player2].hitpoints = 100;
	data[Aircraft::Player2].speed = 200.f;
	data[Aircraft::Player2].fireInterval = sf::seconds(1.5f);
	data[Aircraft::Player2].texture = Textures::Entities;
	data[Aircraft::Player2].textureRect = sf::IntRect(0, 0, 32, 32);
	data[Aircraft::Player2].hasRollAnimation = false;

	data[Aircraft::Raptor].hitpoints = 20;
	data[Aircraft::Raptor].speed = 80.f;
	data[Aircraft::Raptor].texture = Textures::Entities;
	data[Aircraft::Raptor].textureRect = sf::IntRect(144, 0, 84, 64);
	data[Aircraft::Raptor].directions.push_back(Direction(+45.f, 80.f));
	data[Aircraft::Raptor].directions.push_back(Direction(-45.f, 160.f));
	data[Aircraft::Raptor].directions.push_back(Direction(+45.f, 80.f));
	data[Aircraft::Raptor].fireInterval = sf::Time::Zero;
	data[Aircraft::Raptor].hasRollAnimation = false;

	data[Aircraft::Avenger].hitpoints = 40;
	data[Aircraft::Avenger].speed = 50.f;
	data[Aircraft::Avenger].texture = Textures::Entities;
	data[Aircraft::Avenger].textureRect = sf::IntRect(228, 0, 60, 59);
	data[Aircraft::Avenger].directions.push_back(Direction(+45.f,  50.f));
	data[Aircraft::Avenger].directions.push_back(Direction(  0.f,  50.f));
	data[Aircraft::Avenger].directions.push_back(Direction(-45.f, 100.f));
	data[Aircraft::Avenger].directions.push_back(Direction(  0.f,  50.f));
	data[Aircraft::Avenger].directions.push_back(Direction(+45.f,  50.f));
	data[Aircraft::Avenger].fireInterval = sf::seconds(5);
	data[Aircraft::Avenger].hasRollAnimation = false;

	return data;
}

std::vector<ProjectileData> initializeProjectileData()
{
	std::vector<ProjectileData> data(Projectile::TypeCount);

	data[Projectile::AlliedBullet].damage = 10;
	data[Projectile::AlliedBullet].speed = 300.f;
	data[Projectile::AlliedBullet].texture = Textures::Entities;
	data[Projectile::AlliedBullet].textureRect = sf::IntRect(102, 0, 37, 37);

	data[Projectile::EnemyBullet].damage = 10;
	data[Projectile::EnemyBullet].speed = 300.f;
	data[Projectile::EnemyBullet].texture = Textures::Entities;
	data[Projectile::EnemyBullet].textureRect = sf::IntRect(178, 64, 3, 14);

	data[Projectile::Missile].damage = 200;
	data[Projectile::Missile].speed = 150.f;
	data[Projectile::Missile].texture = Textures::Entities;
	data[Projectile::Missile].textureRect = sf::IntRect(160, 64, 15, 32);

	data[Projectile::Ball].texture = Textures::Entities;
	data[Projectile::Ball].speed = 0.f;
	data[Projectile::Ball].textureRect = sf::IntRect(65, 2, 27, 27);
	return data;
}

std::vector<PickupData> initializePickupData()
{
	std::vector<PickupData> data(Pickup::TypeCount);
	
	data[Pickup::HealthRefill].texture = Textures::Entities;
	data[Pickup::HealthRefill].textureRect = sf::IntRect(0, 64, 40, 40);
	data[Pickup::HealthRefill].action = [] (Aircraft& a) { a.repair(25); };

	return data;
}

std::vector<ParticleData> initializeParticleData()
{
	std::vector<ParticleData> data(Particle::ParticleCount);

	data[Particle::Propellant].color = sf::Color(255, 187, 0);
	data[Particle::Propellant].lifetime = sf::seconds(0.3f);

	data[Particle::Smoke].color = sf::Color(255, 0, 0); 
	data[Particle::Smoke].lifetime = sf::seconds(0.6f);

	return data;
}

std::vector<WallData> initializeWallData()
{
	std::vector<WallData> data(Wall::TypeCount);

	data[Wall::HorizontalWall].texture = Textures::HorizontalWall;
	data[Wall::HorizontalWall].speed = 0.f;
	data[Wall::HorizontalWall].textureRect = sf::IntRect(0, 0, 1024, 25);

	data[Wall::VerticalWall].texture = Textures::HorizontalWall;
	data[Wall::VerticalWall].speed = 0.f;
	data[Wall::VerticalWall].textureRect = sf::IntRect(0, 0, 1024, 25);

	data[Wall::AlliedObjective].texture = Textures::Entities;
	data[Wall::AlliedObjective].speed = 0.f;
	data[Wall::AlliedObjective].textureRect = sf::IntRect(0, 64, 285, 154);

	data[Wall::AlliedObjectiveProgress].texture = Textures::Entities;
	data[Wall::AlliedObjectiveProgress].speed = 0.f;
	data[Wall::AlliedObjectiveProgress].textureRect = sf::IntRect(285, 64, 0, 154);

	data[Wall::EnemyObjective].texture = Textures::Entities;
	data[Wall::EnemyObjective].speed = 0.f;
	data[Wall::EnemyObjective].textureRect = sf::IntRect(0, 228, 285, 154);

	data[Wall::EnemyObjectiveProgress].texture = Textures::Entities;
	data[Wall::EnemyObjectiveProgress].speed = 0.f;
	data[Wall::EnemyObjectiveProgress].textureRect = sf::IntRect(285, 228, 0, 154);

	return data;
}
