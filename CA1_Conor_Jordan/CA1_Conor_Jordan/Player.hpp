#ifndef BOOK_PLAYER_HPP
#define BOOK_PLAYER_HPP

#include "Command.hpp"
#include "KeyBinding.hpp"
#include "Controller.hpp"
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Network/TcpSocket.hpp>

#include <map>


class CommandQueue;

class Player : private sf::NonCopyable
{
	public:
		typedef PlayerAction::Type Action;

		enum MissionStatus
		{
			MissionRunning,
			MissionSuccess,
			MissionFailure
		};

		enum class JoystickButton { Square, X, Circle, Triangle, LB, RB, LT, RT, options, start, L3, R3, home, BigButton};
	public:
								Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding);

		void					handleEvent(const sf::Event& event, CommandQueue& commands);
		void					handleRealtimeInput(CommandQueue& commands);
		void					handleRealtimeNetworkInput(CommandQueue& commands);

		// React to events or realtime state changes received over the network
		void					handleNetworkEvent(Action action, CommandQueue& commands);
		void					handleNetworkRealtimeChange(Action action, bool actionEnabled);

		void 					setMissionStatus(MissionStatus status);
		MissionStatus 			getMissionStatus() const;

		void					disableAllRealtimeActions();
		bool					isLocal() const;
		sf::TcpSocket*			getSocket();

	private:
		void					initializeActions();
		void					setJoystick(Controller* joystick);

private:
		int mLocalIdentifier;
		Controller* mJoystick;
		unsigned int mScore;
		std::map<JoystickButton, Action> mJoystickBindingPressed;
		std::map<JoystickButton, Action> mJoystickBindingReleased;
		const KeyBinding*			mKeyBinding;
		std::map<Action, Command>	mActionBinding;
		std::map<Action, bool>		mActionProxies;
		MissionStatus 				mCurrentMissionStatus;
		int							mIdentifier;
		sf::TcpSocket*				mSocket;
};

#endif // BOOK_PLAYER_HPP
