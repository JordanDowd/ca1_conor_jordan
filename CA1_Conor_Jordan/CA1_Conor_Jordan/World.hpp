#ifndef BOOK_WORLD_HPP
#define BOOK_WORLD_HPP

#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"
#include "SpriteNode.hpp"
#include "Aircraft.hpp"
#include "Wall.hpp"
#include "CommandQueue.hpp"
#include "Command.hpp"
#include "Pickup.hpp"
#include "BloomEffect.hpp"
#include "SoundPlayer.hpp"
#include "MusicPlayer.hpp"
#include "NetworkProtocol.hpp"
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <array>
#include <queue>


// Forward declaration
namespace sf
{
	class RenderTarget;
}

class NetworkNode;

class World : private sf::NonCopyable
{
	public:
											World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, MusicPlayer& music, bool networked = false);
		void								update(sf::Time dt);
		void								draw();

		sf::FloatRect						getViewBounds() const;		
		CommandQueue&						getCommandQueue();
		Aircraft*							addAircraft(int identifier);

		Projectile *						addBall(int identifier);
		
		void								removeAircraft(int identifier);
		void								setCurrentBattleFieldPosition(float lineY);
		void								setWorldHeight(float height);

		void								addEnemy(Aircraft::Type type, float relX, float relY);	
		void								sortEnemies();

		void								addBorder(Wall::Type type, float relX, float relY);

		void								addBall(Projectile::Type type, float relX, float relY);

		bool 								hasAlivePlayer() const;
		void								setWorldScrollCompensation(float compensation);
		bool								playerBallCollisionOccured();
		void								setPlayerBallCollisionOccured(bool value, Aircraft* player);
		Aircraft*							getAircraft(int identifier) const;
		Wall*								getProgressBar(Wall::Type type) const;
		Projectile *						GetBall(int identifier) const;
		sf::FloatRect						getBattlefieldBounds() const;

		float								getAlliedProgressBarValue();
		float								getEnemyProgressBarValue();		

		void								createPickup(sf::Vector2f position, Pickup::Type type);
		bool								pollGameAction(GameActions::Action& out);

		bool								mPlayer1Won;
		bool								mPlayer2Won;
		Aircraft*							getPlayerHolding();
	private:
		void								loadTextures();
		void								adaptPlayerPosition();
		void								adaptPlayerVelocity();
		void								handleCollisions();
		void								updateSounds();
		void								checkProgress();

		void								buildScene();
		void								addEnemies();
		void								spawnEnemies();

		void								addBorders();
		void								spawnBorders();

		void								destroyEntitiesOutsideView();
		void								guideMissiles();


	private:
		enum Layer
		{
			Background,
			LowerAir,
			UpperAir,
			LayerCount
		};

		struct SpawnPoint 
		{
			SpawnPoint(Aircraft::Type type, float x, float y)
			: type(type)
			, x(x)
			, y(y)
			{
			}

			Aircraft::Type type;
			float x;
			float y;
		};

		struct BallSpawnPoint
		{
			BallSpawnPoint(Projectile::Type type, float x, float y)
				: type(type)
				, x(x)
				, y(y)
			{
			}

			Projectile::Type type;
			float x;
			float y;
		};

		struct BorderSpawnPoint
		{
			BorderSpawnPoint(Wall::Type type, float x, float y)
				: type(type)
				, x(x)
				, y(y)
			{
			}

			Wall::Type type;
			float x;
			float y;
		};

	private:
		sf::RenderTarget&					mTarget;
		sf::RenderTexture					mSceneTexture;
		sf::View							mWorldView;
		TextureHolder						mTextures;
		FontHolder&							mFonts;
		SoundPlayer&						mSounds;
		MusicPlayer&						mMusic;
		SceneNode							mSceneGraph;
		std::array<SceneNode*, LayerCount>	mSceneLayers;
		CommandQueue						mCommandQueue;

		sf::FloatRect						mWorldBounds;
		sf::Vector2f						mSpawnPosition;
		float								mScrollSpeed;
		float								mScrollSpeedCompensation;
		std::vector<Aircraft*>				mPlayerAircrafts;
		std::vector<Wall*>					mProgressBars;
		std::vector<Projectile*>			mBalls;
		std::vector<SpawnPoint>				mEnemySpawnPoints;
		std::vector<Aircraft*>				mActiveEnemies;

		std::vector<BorderSpawnPoint>	    mBorderSpawnPoints;
		std::vector<BallSpawnPoint>			mBallSpawnPoints;
		BloomEffect							mBloomEffect;
		bool								mPlayerBallCollisionOccured;
		bool								mNetworkedWorld;
		NetworkNode*						mNetworkNode;
		SpriteNode*							mFinishSprite;
		Aircraft*							mPlayerHolding;
		float								mAlliedProgressValue;
		float								mEnemyProgressValue;
};

#endif // BOOK_WORLD_HPP
