#ifndef BOOK_AIRCRAFT_HPP
#define BOOK_AIRCRAFT_HPP

#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Projectile.hpp"
#include "Wall.hpp"
#include "TextNode.hpp"
#include "Animation.hpp"

#include <SFML/Graphics/Sprite.hpp>
class Projectile;

class Aircraft : public Entity
{
	public:
		enum Type
		{
			Eagle,
			Player2,
			HorizontalWall,
			VerticalWall,
			Raptor,
			Avenger,
			TypeCount
		};


	public:
								Aircraft(Type type, const TextureHolder& textures, const FontHolder& fonts);

		virtual unsigned int	getCategory() const;
		virtual sf::FloatRect	getBoundingRect() const;
		virtual void			remove();
		virtual bool 			isMarkedForRemoval() const;
		bool					isAllied() const;
		float					getMaxSpeed() const;
		void					disablePickups();

		//void					increaseFireRate();
		//void					increaseSpread();
		//void					collectMissiles(unsigned int count);

		void 					fire();
		void					launchMissile();
		void					playLocalSound(CommandQueue& commands, SoundEffect::ID effect);
		int						getIdentifier();
		void					setIdentifier(int identifier);
		void					setIsFiring(bool firing);
		void					setBallCanMove(bool move);
		void					setIntercepted(bool intercepted);
		void					setIsBallCaught(Projectile* ball, bool caught);
		void					setIsBallCaught(bool caught);
		Projectile*				getBall();
		bool					isBallCaught();
		bool					isFiring();
		bool					ballCanMove();
		bool					ballCollided();
		bool					wasIntercepted();
		void					ballCollidedWithPlayer();
		sf::Vector2f			getCenterPosition();
		void					updateTexture();

	private:
		virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
		virtual void 			updateCurrent(sf::Time dt, CommandQueue& commands);
		void					updateMovementPattern(sf::Time dt);
		void					checkPickupDrop(CommandQueue& commands);
		void					checkProjectileLaunch(sf::Time dt, CommandQueue& commands);

		void					createBullets(SceneNode& node, const TextureHolder& textures) const;
		void					createProjectile(SceneNode& node, float xOffset, float yOffset, const TextureHolder& textures) const;
		void					createPickup(SceneNode& node, const TextureHolder& textures) const;
		void					dealWithBall();

		void					updateTexts();
		void					updateRollAnimation();
		

	private:
		Type					mType;
		sf::Sprite				mSprite;
		Animation				mExplosion;
		Command 				mFireCommand;
		Command					mMissileCommand;
		Command					mCaughtCommand;
		sf::Time				mFireCountdown;
		bool 					mIsFiring;
		bool					mBallCanMove;
		bool					mIsLaunchingMissile;
		bool 					mShowExplosion;
		bool					mExplosionBegan;
		bool					mSpawnedPickup;
		bool					mPickupsEnabled;
		bool					mIntercepted;
		bool					mBallCollided;
		bool					mBallCaught;
		int						mFireRateLevel;
		int						mSpreadLevel;
		int						mMissileAmmo;

		Command 				mDropPickupCommand;
		float					mTravelledDistance;
		std::size_t				mDirectionIndex;
		TextNode*				mHealthDisplay;
		TextNode*				mMissileDisplay;
		Projectile*				    mBall;
		int						mIdentifier;
};

#endif // BOOK_AIRCRAFT_HPP
