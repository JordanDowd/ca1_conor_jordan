#ifndef BOOK_PROJECTILE_HPP
#define BOOK_PROJECTILE_HPP

#include "Entity.hpp"
#include "ResourceIdentifiers.hpp"
#include "Aircraft.hpp"
#include <SFML/Graphics/Sprite.hpp>

class Aircraft;
class Projectile : public Entity
{
	public:
		enum Type
		{
			Ball,
			AlliedBullet,
			EnemyBullet,
			Missile,
			TypeCount
		};


	public:
								Projectile(Type type, const TextureHolder& textures);

		void					guideTowards(sf::Vector2f position);
		bool					isGuided() const;
		int						getIdentifier();
		void					setIdentifier(int identifier);
		virtual unsigned int	getCategory() const;
		virtual sf::FloatRect	getBoundingRect() const;
		float					getMaxSpeed() const;
		int						getDamage() const;
		void					setPlayerCaught(Aircraft* player, bool caught);
		Aircraft*				getPlayerCaught();
		bool					isBallCaught();
		void					setPlayerCaught(bool caught);
		float					getBallValue() const;
		void					setBallValue(float value);
		virtual unsigned int	getType() const;
		void					playLocalSound(CommandQueue & commands, SoundEffect::ID effect);
		void					setHitWall(bool hit);
	private:
		virtual void			updateCurrent(sf::Time dt, CommandQueue& commands);
		virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;


	private:
		Type					mType;
		sf::Sprite				mSprite;
		sf::Vector2f			mTargetDirection;
		Aircraft*				mPlayer;
		bool					mBallIsCaught;
		float					mBallValue;
		bool					mHitWall;
		int						mIdentifier;
};

#endif // BOOK_PROJECTILE_HPP
