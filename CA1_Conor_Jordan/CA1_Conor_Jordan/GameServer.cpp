#include "GameServer.hpp"
#include "NetworkProtocol.hpp"
#include "Foreach.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "Aircraft.hpp"
#include "Constants.hpp"
#include <SFML/Network/Packet.hpp>

#include  <iostream>
GameServer::RemotePeer::RemotePeer() 
: ready(false)
, timedOut(false)
{
	socket.setBlocking(false);
}

GameServer::GameServer(sf::Vector2f battlefieldSize)
	: mThread(&GameServer::executionThread, this)
	, mListeningState(false)
	, mClientTimeoutTime(sf::seconds(3.f))
	, mMaxConnectedPlayers(10)
	, mConnectedPlayers(0)
	, mWorldHeight(5000.f)
	, mBattleFieldRect(0.f, mWorldHeight - battlefieldSize.y, battlefieldSize.x, battlefieldSize.y)
	, mAircraftCount(0)
	, mBallCount(0)
	, mPeers(1)
	, mAircraftIdentifierCounter(1)
	, mBallIdentifierCounter(0)
	, mPlayerSpawnOffset(40)
	,mAlliedProgressCount(0)
	,mEnemyProgressCount(0)
, mWaitingThreadEnd(false)
, mLastSpawnTime(sf::Time::Zero)
, mTimeForNextSpawn(sf::seconds(5.f))
{
	//This is used to position the players in the y position during the start of the game
	mGreenSpawnPosY = mBattleFieldRect.top + mBattleFieldRect.height / 2;
	mRedSpawnPosY = mGreenSpawnPosY;

	mListenerSocket.setBlocking(false);
	mPeers[0].reset(new RemotePeer());
	mThread.launch();
}

GameServer::~GameServer()
{
	mWaitingThreadEnd = true;
	mThread.wait();
}

void GameServer::notifyPlayerRealtimeChange(sf::Int32 aircraftIdentifier, sf::Int32 action, bool actionEnabled)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerRealtimeChange);
			packet << aircraftIdentifier;
			packet << action;
			packet << actionEnabled;

			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::notifyPlayerEvent(sf::Int32 aircraftIdentifier, sf::Int32 action)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerEvent);
			packet << aircraftIdentifier;
			packet << action;

			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::notifyPlayerSpawn(sf::Int32 aircraftIdentifier)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerConnect);
			packet << aircraftIdentifier << mAircraftInfo[aircraftIdentifier].position.x << mAircraftInfo[aircraftIdentifier].position.y;
			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::setListening(bool enable)
{
	// Check if it isn't already listening
	if (enable)
	{	
		if (!mListeningState)
			mListeningState = (mListenerSocket.listen(ServerPort) == sf::TcpListener::Done);
	}
	else
	{
		mListenerSocket.close();
		mListeningState = false;
	}
}

void GameServer::executionThread()
{
	setListening(true);

	sf::Time stepInterval = sf::seconds(1.f / 60.f);
	sf::Time stepTime = sf::Time::Zero;
	sf::Time tickInterval = sf::seconds(1.f / 20.f);
	sf::Time tickTime = sf::Time::Zero;
	sf::Clock stepClock, tickClock;

	while (!mWaitingThreadEnd)
	{	
		handleIncomingPackets();
		handleIncomingConnections();

		stepTime += stepClock.getElapsedTime();
		stepClock.restart();

		tickTime += tickClock.getElapsedTime();
		tickClock.restart();

		// Fixed tick step
		while (tickTime >= tickInterval)
		{
			tick();
			tickTime -= tickInterval;
		}

		// Sleep to prevent server from consuming 100% CPU
		sf::sleep(sf::milliseconds(100));
	}	
}

void GameServer::tick()
{
	updateClientState();

	//TODO: loop through zones and check if full to get a mission success
	// Check for mission success = all planes with position.y < offset
	bool allAircraftsDone = true;
	if (mAlliedProgressCount < FINISH_VALUE && mEnemyProgressCount < FINISH_VALUE)
	{
		allAircraftsDone = false;
	}
	if (allAircraftsDone)
	{
		sf::Packet missionSuccessPacket;
		missionSuccessPacket << static_cast<sf::Int32>(Server::MissionSuccess);
		sendToAll(missionSuccessPacket);
	}

	//// Check if its time to attempt to spawn enemies
	//if (now() >= mTimeForNextSpawn + mLastSpawnTime)
	//{	
	//	// No more enemies are spawned near the end
	//	if (mBattleFieldRect.top > 600.f)
	//	{
	//		std::size_t enemyCount = 1u + randomInt(2);
	//		float spawnCenter = static_cast<float>(randomInt(500) - 250);

	//		// In case only one enemy is being spawned, it appears directly at the spawnCenter
	//		float planeDistance = 0.f;
	//		float nextSpawnPosition = spawnCenter;
	//		
	//		// In case there are two enemies being spawned together, each is spawned at each side of the spawnCenter, with a minimum distance
	//		if (enemyCount == 2)
	//		{
	//			planeDistance = static_cast<float>(150 + randomInt(250));
	//			nextSpawnPosition = spawnCenter - planeDistance / 2.f;
	//		}

	//		// Send the spawn orders to all clients
	//		for (std::size_t i = 0; i < enemyCount; ++i)
	//		{
	//			sf::Packet packet;
	//			packet << static_cast<sf::Int32>(Server::SpawnEnemy);
	//			packet << static_cast<sf::Int32>(1 + randomInt(Aircraft::TypeCount-1));
	//			packet << mWorldHeight - mBattleFieldRect.top + 500;
	//			packet << nextSpawnPosition;

	//			nextSpawnPosition += planeDistance / 2.f;

	//			sendToAll(packet);
	//		}

	//		mLastSpawnTime = now();
	//		mTimeForNextSpawn = sf::milliseconds(2000 + randomInt(6000));
	//	}
	//}
}

sf::Time GameServer::now() const
{
	return mClock.getElapsedTime();
}

void GameServer::handleIncomingPackets()
{
	bool detectedTimeout = false;
	
	FOREACH(PeerPtr& peer, mPeers)
	{
		if (peer->ready)
		{
			sf::Packet packet;
			while (peer->socket.receive(packet) == sf::Socket::Done)
			{
				// Interpret packet and react to it
				handleIncomingPacket(packet, *peer, detectedTimeout);

				// Packet was indeed received, update the ping timer
				peer->lastPacketTime = now();
				packet.clear();
			}

			if (now() >= peer->lastPacketTime + mClientTimeoutTime)
			{
				peer->timedOut = true;
				detectedTimeout = true;
			}
		}
	}

	if (detectedTimeout)
		handleDisconnections();
}

void GameServer::handleIncomingPacket(sf::Packet& packet, RemotePeer& receivingPeer, bool& detectedTimeout)
{
	sf::Int32 packetType;
	packet >> packetType;

	switch (packetType)
	{
		case Client::Quit:
		{
			receivingPeer.timedOut = true;
			detectedTimeout = true;
		} break;

		case Client::PlayerEvent:
		{
			sf::Int32 aircraftIdentifier;
			sf::Int32 action;
			packet >> aircraftIdentifier >> action;

			notifyPlayerEvent(aircraftIdentifier, action);
		} break;

		case Client::PlayerRealtimeChange:
		{
			sf::Int32 aircraftIdentifier;
			sf::Int32 action;
			bool actionEnabled;
			packet >> aircraftIdentifier >> action >> actionEnabled;
			mAircraftInfo[aircraftIdentifier].realtimeActions[action] = actionEnabled;
			notifyPlayerRealtimeChange(aircraftIdentifier, action, actionEnabled);
		} break;

		case Client::RequestSpawnBall:
		{
			receivingPeer.ballIdentifiers.push_back(mBallIdentifierCounter);
			mBallInfo[mBallIdentifierCounter].position = sf::Vector2f(mBattleFieldRect.width / 2, mBattleFieldRect.top + mBattleFieldRect.height / 2);

			sf::Packet requestPacket;
			requestPacket << static_cast<sf::Int32>(Server::AcceptSpawnBall);
			requestPacket << mBallIdentifierCounter;
			requestPacket << mBallInfo[mBallIdentifierCounter].position.x;
			requestPacket << mBallInfo[mBallIdentifierCounter].position.y;

			receivingPeer.socket.send(requestPacket);
			mBallCount++;

			// Inform every other peer about this new plane
			FOREACH(PeerPtr& peer, mPeers)
			{
				if (peer.get() != &receivingPeer && peer->ready)
				{
					sf::Packet notifyPacket;
					notifyPacket << static_cast<sf::Int32>(Server::BallConnect);
					notifyPacket << mBallIdentifierCounter;
					notifyPacket << mBallInfo[mBallIdentifierCounter].position.x;
					notifyPacket << mBallInfo[mBallIdentifierCounter].position.y;
					peer->socket.send(notifyPacket);
				}
			}
			mBallIdentifierCounter++;
		} break;

		case Client::ResetBalls:
		{
			// Inform every other peer about this new plane
			FOREACH(PeerPtr& peer, mPeers)
			{
				if (peer->ready)
				{
					sf::Packet notifyPacket;
					notifyPacket << static_cast<sf::Int32>(Server::AcceptResetBalls);
					peer->socket.send(notifyPacket);
				}
			}

		} break;

		case Client::StartGame:
		{
			if (&receivingPeer == mPeers[0].get())
			{
				sf::Packet startGame;
				startGame << static_cast<sf::Int32>(Server::StartGame);

				sendToAll(startGame);
			}
		} break;

		case Client::ProgressBarUpdate:
		{
			if (&receivingPeer == mPeers[0].get())
			{

				packet >> mAlliedProgressCount;
				packet >> mEnemyProgressCount;

				
				sf::Packet ProgressBarUpdate;
				ProgressBarUpdate << static_cast<sf::Int32>(Server::ProgressBarUpdate);
				ProgressBarUpdate << mAlliedProgressCount;
				ProgressBarUpdate << mEnemyProgressCount;

				sendToAll(ProgressBarUpdate);
			}
		} break;

		case Client::PositionUpdate:
		{
			sf::Int32 numAircrafts;
			packet >> numAircrafts;

			for (sf::Int32 i = 0; i < numAircrafts; ++i)
			{
				sf::Int32 aircraftIdentifier;
				sf::Vector2f aircraftPosition;
				packet >> aircraftIdentifier >> aircraftPosition.x >> aircraftPosition.y;
				mAircraftInfo[aircraftIdentifier].position = aircraftPosition;
			}


		} break;
		case Client::BallPositionUpdate:
		{
			sf::Int32 numBalls;
			packet >> numBalls;
				for (sf::Int32 i = 0; i < numBalls; ++i)
				{
					sf::Int32 BallIdentifier;
					sf::Vector2f BallPosition;
					packet >> BallIdentifier >> BallPosition.x >> BallPosition.y;
					mBallInfo[BallIdentifier].position = BallPosition;

				}

		} break;

		case Client::UpdateBallCaught:
		{
			//if (&receivingPeer == mPeers[0].get())
			//{
				sf::Int32 ballID;
				sf::Int32 playerID;
				packet >> /*ballID >> */playerID;

				sf::Packet newPacket;
				newPacket << static_cast<sf::Int32>(Server::UpdateBallCaughtAccepted);
				//newPacket << ballID;
				newPacket << playerID;

				sendToAll(newPacket);
			//}
		} break;

		case Client::GameEvent:
		{
			sf::Int32 action;
			float x;
			float y;

			packet >> action;
			packet >> x;
			packet >> y;

			// Enemy explodes: With certain probability, drop pickup
			// To avoid multiple messages spawning multiple pickups, only listen to first peer (host)
			if (action == GameActions::EnemyExplode && randomInt(3) == 0 && &receivingPeer == mPeers[0].get())
			{
				sf::Packet packet;
				packet << static_cast<sf::Int32>(Server::SpawnPickup);
				packet << static_cast<sf::Int32>(randomInt(Pickup::TypeCount));
				packet << x;
				packet << y;

				sendToAll(packet);
			}
		}
	}
}

void GameServer::updateClientState()
{
	sf::Packet updateClientStatePacket;
	updateClientStatePacket << static_cast<sf::Int32>(Server::UpdateClientState);
	updateClientStatePacket << static_cast<sf::Int32>(mAircraftInfo.size());

	FOREACH(auto aircraft, mAircraftInfo)
		updateClientStatePacket << aircraft.first << aircraft.second.position.x << aircraft.second.position.y;

	updateClientStatePacket << static_cast<sf::Int32>(mBallInfo.size());

	FOREACH(auto ball, mBallInfo)
		updateClientStatePacket << ball.first << ball.second.position.x << ball.second.position.y;
	sendToAll(updateClientStatePacket);
}

void GameServer::handleIncomingConnections()
{
	if (!mListeningState)
		return;

	if (mListenerSocket.accept(mPeers[mConnectedPlayers]->socket) == sf::TcpListener::Done)
	{
		// order the new client to spawn its own plane and every other player connected
		// if the players identifier is even, they will spawn on the green team
		// We then have an offset to prevent players from spawning on top of each other
		if (mAircraftIdentifierCounter % 2 == 0)
		{
			mAircraftInfo[mAircraftIdentifierCounter].position = sf::Vector2f(910.f, mGreenSpawnPosY);
			mGreenSpawnPosY = mGreenSpawnPosY + mPlayerSpawnOffset;
		}			
		else
		{
			mAircraftInfo[mAircraftIdentifierCounter].position = sf::Vector2f(110.f, mRedSpawnPosY);
			mRedSpawnPosY = mRedSpawnPosY + mPlayerSpawnOffset;
		}

		sf::Packet packet;
		packet << static_cast<sf::Int32>(Server::SpawnSelf);
		packet << mAircraftIdentifierCounter;
		packet << mAircraftInfo[mAircraftIdentifierCounter].position.x;
		packet << mAircraftInfo[mAircraftIdentifierCounter].position.y;
		
		mPeers[mConnectedPlayers]->aircraftIdentifiers.push_back(mAircraftIdentifierCounter);
		
		broadcastMessage("New player!");
		informWorldState(mPeers[mConnectedPlayers]->socket);
		notifyPlayerSpawn(mAircraftIdentifierCounter++);

		mPeers[mConnectedPlayers]->socket.send(packet);
		mPeers[mConnectedPlayers]->ready = true;
		mPeers[mConnectedPlayers]->lastPacketTime = now(); // prevent initial timeouts
		mAircraftCount++;
		mConnectedPlayers++;

		if (mConnectedPlayers >= mMaxConnectedPlayers)
			setListening(false);
		else // Add a new waiting peer
			mPeers.push_back(PeerPtr(new RemotePeer()));
	}
}

void GameServer::handleDisconnections()
{
	for (auto itr = mPeers.begin(); itr != mPeers.end(); )
	{
		if ((*itr)->timedOut)
		{
			// Inform everyone of the disconnection, erase 
			FOREACH(sf::Int32 identifier, (*itr)->aircraftIdentifiers)
			{
				sendToAll(sf::Packet() << static_cast<sf::Int32>(Server::PlayerDisconnect) << identifier);

				mAircraftInfo.erase(identifier);
			}

			mConnectedPlayers--;
			mAircraftCount -= (*itr)->aircraftIdentifiers.size();

			itr = mPeers.erase(itr);

			// Go back to a listening state if needed
			if (mConnectedPlayers < mMaxConnectedPlayers)
			{
				mPeers.push_back(PeerPtr(new RemotePeer()));
				setListening(true);
			}
				
			broadcastMessage("An ally has disconnected.");
		}
		else
		{
			++itr;
		}
	}
}

// Tell the newly connected peer about how the world is currently
void GameServer::informWorldState(sf::TcpSocket& socket)
{
	sf::Packet packet;
	packet << static_cast<sf::Int32>(Server::InitialState);
	packet << mWorldHeight << mBattleFieldRect.top + mBattleFieldRect.height;
	packet << static_cast<sf::Int32>(mAircraftCount);

	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			FOREACH(sf::Int32 identifier, mPeers[i]->aircraftIdentifiers)
				packet << identifier << mAircraftInfo[identifier].position.x << mAircraftInfo[identifier].position.y;
		}
	}

	packet << static_cast<sf::Int32>(mBallCount);

	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			FOREACH(sf::Int32 identifier, mPeers[i]->ballIdentifiers)
				packet << identifier << mBallInfo[identifier].position.x << mBallInfo[identifier].position.y;
		}
	}

	socket.send(packet);
}

void GameServer::broadcastMessage(const std::string& message)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::BroadcastMessage);
			packet << message;

			mPeers[i]->socket.send(packet);
		}	
	}
}

void GameServer::sendToAll(sf::Packet& packet)
{
	FOREACH(PeerPtr& peer, mPeers)
	{
		if (peer->ready)
			peer->socket.send(packet);
	}
}
