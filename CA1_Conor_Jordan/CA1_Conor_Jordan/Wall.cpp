#include "Wall.hpp"
#include "DataTables.hpp"
#include "Category.hpp"
#include "CommandQueue.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

namespace
{
	const std::vector<WallData> Table = initializeWallData();
}

Wall::Wall(Type type, const TextureHolder& textures)
	: Entity(1)
	, mType(type)
	, mSprite(textures.get(Table[type].texture), Table[type].textureRect)
	, mValue(0)
	, mIdentifier(0)
{
	centerOrigin(mSprite);
}

unsigned int Wall::getCategory() const
{
	return Category::Wall;
}

sf::FloatRect Wall::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

unsigned int Wall::getType() const
{
	return mType;
}

void Wall::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}

void Wall::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	Entity::updateCurrent(dt, commands);
}


void Wall::updateTexture(float val)
{

	sf::IntRect textureRect = Table[mType].textureRect;
	textureRect.width += val;
	mSprite.setTextureRect(textureRect);
}

int	Wall::getIdentifier()
{
	return mIdentifier;
}

void Wall::setIdentifier(int identifier)
{
	mIdentifier = identifier;
}
