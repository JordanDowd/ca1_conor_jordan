#include "World.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "Foreach.hpp"
#include "TextNode.hpp"
#include "ParticleNode.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "Utility.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include "NetworkProtocol.hpp"
#include "Constants.hpp"
#include <SFML/Network/Packet.hpp>
#include <algorithm>
#include <cmath>
#include <limits>
#include <iostream>

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, MusicPlayer& music, bool networked)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mMusic(music)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, 5000.f)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mScrollSpeed(-50.f)
	, mScrollSpeedCompensation(1.f)
	, mPlayerAircrafts()
	, mProgressBars()
	, mBalls()
	, mEnemySpawnPoints()
	, mBorderSpawnPoints()
	, mActiveEnemies()
	, mNetworkedWorld(networked)
	, mNetworkNode(nullptr)
	, mFinishSprite(nullptr)
	, mAlliedProgressValue(0)
	, mEnemyProgressValue(0)
	, mPlayer1Won(false)
	, mPlayer2Won(false)
	, mPlayerBallCollisionOccured(false)
{
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();

	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
}

void World::setWorldScrollCompensation(float compensation)
{
	mScrollSpeedCompensation = compensation;
}

bool World::playerBallCollisionOccured()
{
	return mPlayerBallCollisionOccured;
}

void World::setPlayerBallCollisionOccured(bool value, Aircraft* player)
{
	mPlayerBallCollisionOccured = value;
	mPlayerHolding = player;
}


void World::update(sf::Time dt)
{
	// Scroll the world, reset player velocity
	//mWorldView.move(0.f, mScrollSpeed * dt.asSeconds() * mScrollSpeedCompensation);	

	FOREACH(Aircraft* a, mPlayerAircrafts)
		a->setVelocity(a->getVelocity() *0.4f);

	// Setup commands to destroy entities, and guide missiles
	destroyEntitiesOutsideView();
	guideMissiles();

	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	//adaptPlayerVelocity();

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove aircrafts that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in mPlayerAircraft)
	auto firstToRemove = std::remove_if(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), std::mem_fn(&Aircraft::isMarkedForRemoval));
	mPlayerAircrafts.erase(firstToRemove, mPlayerAircrafts.end());

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();
	spawnEnemies();

	spawnBorders();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptPlayerPosition();

	updateSounds();
}

void World::draw()
{
	if (PostEffect::isSupported())
	{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mBloomEffect.apply(mSceneTexture, mTarget);
	}
	else
	{
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
	}
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

Aircraft* World::getAircraft(int identifier) const
{
	FOREACH(Aircraft* a, mPlayerAircrafts)
	{
		if (a->getIdentifier() == identifier)
			return a;
	}

	return nullptr;
}

Wall* World::getProgressBar(Wall::Type type) const
{
	FOREACH(Wall* a, mProgressBars)
	{
		if (a->getType() == type)
			return a;
	}

	return nullptr;
}

Projectile* World::GetBall(int identifier) const
{
	FOREACH(Projectile* b, mBalls)
	{
		if (b->getIdentifier() == identifier)
			return b;
	}

	return nullptr;
}

void World::removeAircraft(int identifier)
{
	Aircraft* aircraft = getAircraft(identifier);
	if (aircraft)
	{
		aircraft->destroy();
		mPlayerAircrafts.erase(std::find(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), aircraft));
	}
}

Aircraft* World::addAircraft(int identifier)
{
	if (identifier % 2 == 0)
	{
		std::unique_ptr<Aircraft> player(new Aircraft(Aircraft::Eagle, mTextures, mFonts));
		player->setPosition(910.f, mWorldView.getCenter().y);
		player->setIdentifier(identifier);
		mPlayerAircrafts.push_back(player.get());
		mSceneLayers[UpperAir]->attachChild(std::move(player));
	}
	else
	{
		std::unique_ptr<Aircraft> player(new Aircraft(Aircraft::Player2, mTextures, mFonts));
		player->setPosition(110.f, mWorldView.getCenter().y);
		player->setIdentifier(identifier);
		mPlayerAircrafts.push_back(player.get());
		mSceneLayers[UpperAir]->attachChild(std::move(player));
	}
	return mPlayerAircrafts.back();
}

Projectile* World::addBall(int identifier)
{
	std::unique_ptr<Projectile> ball(new Projectile(Projectile::Ball, mTextures));
	ball->setPosition(mWorldView.getCenter().x, mWorldView.getCenter().y);
	ball->setIdentifier(identifier);
	mBalls.push_back(ball.get());
	mSceneLayers[UpperAir]->attachChild(std::move(ball));
	
	return mBalls.back();
}

void World::createPickup(sf::Vector2f position, Pickup::Type type)
{
	std::unique_ptr<Pickup> pickup(new Pickup(type, mTextures));
	pickup->setPosition(position);
	pickup->setVelocity(0.f, 1.f);
	mSceneLayers[UpperAir]->attachChild(std::move(pickup));
}

bool World::pollGameAction(GameActions::Action& out)
{
	return mNetworkNode->pollGameAction(out);
}

void World::setCurrentBattleFieldPosition(float lineY)
{
	mWorldView.setCenter(mWorldView.getCenter().x, lineY - mWorldView.getSize().y / 2);
	mSpawnPosition.y = mWorldBounds.height;
}

void World::setWorldHeight(float height)
{
	mWorldBounds.height = height;
}

bool World::hasAlivePlayer() const
{
	return mPlayerAircrafts.size() > 0;
}


Aircraft * World::getPlayerHolding()
{
	return mPlayerHolding;
}

void World::loadTextures()
{
	mTextures.load(Textures::Entities, "Media/Textures/Objects.png");
	mTextures.load(Textures::HorizontalWall, "Media/Textures/horizontalWall.png");
	mTextures.load(Textures::VerticalWall, "Media/Textures/verticalWall.png");
	mTextures.load(Textures::Jungle, "Media/Textures/Jungle.png");
	mTextures.load(Textures::Explosion, "Media/Textures/Explosion.png");
	mTextures.load(Textures::Particle, "Media/Textures/ball_trail1.png");
	mTextures.load(Textures::FinishLine, "Media/Textures/FinishLine.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 47.f;

	FOREACH(Aircraft* aircraft, mPlayerAircrafts)
	{
		sf::Vector2f position = aircraft->getPosition();
		position.x = std::max(position.x, viewBounds.left + borderDistance);
		position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistance);
		position.y = std::max(position.y, viewBounds.top + borderDistance);
		position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
		aircraft->setPosition(position);
	}
}

void World::adaptPlayerVelocity()
{
	FOREACH(Aircraft* aircraft, mPlayerAircrafts)
	{
		sf::Vector2f velocity = aircraft->getVelocity();

		// If moving diagonally, reduce velocity (to have always same velocity)
		if (velocity.x != 0.f && velocity.y != 0.f)
			aircraft->setVelocity(velocity / std::sqrt(2.f));

		// Add scrolling velocity
		aircraft->accelerate(0.f, mScrollSpeed);
	}
}

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	//std::cout << mMusic.getCurrentMusic().getStatus() << std::endl;
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);
	bool stopSound = true;
	FOREACH(SceneNode::Pair pair, collisionPairs)
	{


		/*if (matchesCategories(pair, Category::PlayerAircraft, Category::PlayerAircraft))
		{
			auto& player1 = static_cast<Aircraft&>(*pair.first);
			auto& player2 = static_cast<Aircraft&>(*pair.second);
			if (player2.isBallCaught() && !player1.isFiring())
			{
				player1.setIsBallCaught(player2.getBall(), true);
				player2.getBall()->setPlayerCaught(&player1, true);
				player2.setIsBallCaught(false);
				player2.setIsFiring(true);
			}
			else if (player1.isBallCaught() && !player2.isFiring())
			{
				player2.setIsBallCaught(player1.getBall(), true);
				player1.getBall()->setPlayerCaught(&player2, true);
				player1.setIsBallCaught(false);
				player1.setIsFiring(true);
			}

		}*/
		if (matchesCategories(pair, Category::PlayerAircraft, Category::Projectile))
		{
			auto& player = static_cast<Aircraft&>(*pair.first);
			auto& objective = static_cast<Projectile&>(*pair.second);
			if (!player.isBallCaught() && !player.ballCanMove())
			{
				
				player.setIsBallCaught(&objective, true);
				player.ballCollidedWithPlayer();
				setPlayerBallCollisionOccured(true, &player);
			}

		}

		//else if (matchesCategories(pair, Category::PlayerAircraft, Category::Pickup))
		//{
		//	auto& player = static_cast<Aircraft&>(*pair.first);
		//	auto& pickup = static_cast<Pickup&>(*pair.second);

		//	// Apply pickup effect to player, destroy projectile
		//	pickup.apply(player);
		//	pickup.destroy();
		//	player.playLocalSound(mCommandQueue, SoundEffect::CollectPickup);
		//}

		//else if (matchesCategories(pair, Category::EnemyAircraft, Category::AlliedProjectile)
		//	|| matchesCategories(pair, Category::PlayerAircraft, Category::EnemyProjectile))
		//{
		//	auto& aircraft = static_cast<Aircraft&>(*pair.first);
		//	auto& projectile = static_cast<Projectile&>(*pair.second);

		//	// Apply projectile damage to aircraft, destroy projectile
		//	aircraft.damage(projectile.getDamage());
		//	projectile.destroy();
		//}

		if (matchesCategories(pair, Category::WallBorders, Category::Projectile))
		{
			auto& wall = static_cast<Wall&>(*pair.first);
			auto& ball = static_cast<Projectile&>(*pair.second);

			float ballValue = ball.getBallValue();
			
			// When the bullet hits the wall
			if (wall.getType() == Wall::HorizontalWall)
			{
				float valY = ball.getVelocity().y;
				ball.setVelocity(ball.getVelocity().x, -valY);
				ball.setHitWall(true);
			}
			if (wall.getType() == Wall::VerticalWall)
			{
				float valX = ball.getVelocity().x;
				ball.setVelocity(-valX, ball.getVelocity().y);
				ball.setHitWall(true);
			}

			
			if (wall.getType() == Wall::AlliedObjective)
			{
				
				Wall* alliedProgressBar = getProgressBar(Wall::AlliedObjectiveProgress);
				
				if (mAlliedProgressValue < FINISH_VALUE)
				{
					if (ball.isBallCaught() && ball.getPlayerCaught()->getIdentifier() % 2 == 0)
					{
						//do nothing
					}
					else
					{
						stopSound = false;
						//alliedProgressBar->updateTexture(mAlliedProgressValue);
						mAlliedProgressValue += ballValue;
						
						if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Stopped)
							mMusic.play(Music::ProgressSound);
						if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Paused)
							mMusic.setPaused(false);
					}
					
				}
				else
				{
					if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Playing)
					mMusic.setPaused(true);
					mPlayer2Won = true;
				}
			}
			if (wall.getType() == Wall::EnemyObjective)
			{
				Wall* enemyProgressBar = getProgressBar(Wall::EnemyObjectiveProgress);

				if (mEnemyProgressValue < FINISH_VALUE)
				{
					if (ball.isBallCaught() && ball.getPlayerCaught()->getIdentifier() % 2 != 0)
					{
						//do nothing

					}
					else
					{
						stopSound = false;
						//enemyProgressBar->updateTexture(mEnemyProgressValue);
						mEnemyProgressValue += ballValue;
						if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Stopped)
							mMusic.play(Music::ProgressSound);
						if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Paused)
						mMusic.setPaused(false);
					}
				}
				else
				{
					if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Playing)
					mMusic.setPaused(true);
					mPlayer1Won = true;
				}
			}
		}
	}

	if (stopSound)
	{
		if (mMusic.getCurrentMusic().getStatus() == sf::SoundSource::Playing)
			mMusic.setPaused(true);
	}
}

void World::updateSounds()
{
	sf::Vector2f listenerPosition;

	// 0 players (multiplayer mode, until server is connected) -> view center
	//if (mPlayerAircrafts.empty())
	//{
		listenerPosition = mWorldView.getCenter();
	//}

	// 1 or more players -> mean position between all aircrafts
	//else
	//{
	//	FOREACH(Aircraft* aircraft, mPlayerAircrafts)
	//		listenerPosition += aircraft->getWorldPosition();

	//	listenerPosition /= static_cast<float>(mPlayerAircrafts.size());
	//}

	// Set listener's position
	mSounds.setListenerPosition(listenerPosition);

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}

void World::checkProgress()
{
	
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	// Prepare the tiled background
	sf::Texture& jungleTexture = mTextures.get(Textures::Jungle);
	jungleTexture.setRepeated(true);

	float viewHeight = mWorldView.getSize().y;
	sf::IntRect textureRect(mWorldBounds);
	textureRect.height += static_cast<int>(viewHeight);

	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> jungleSprite(new SpriteNode(jungleTexture, textureRect));
	jungleSprite->setPosition(mWorldBounds.left, mWorldBounds.top - viewHeight);
	mSceneLayers[Background]->attachChild(std::move(jungleSprite));

	// Add the finish line to the scene
	sf::Texture& finishTexture = mTextures.get(Textures::FinishLine);
	std::unique_ptr<SpriteNode> finishSprite(new SpriteNode(finishTexture));
	finishSprite->setPosition(0.f, -76.f);
	mFinishSprite = finishSprite.get();
	mSceneLayers[Background]->attachChild(std::move(finishSprite));

	// Add particle node to the scene
	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Smoke, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(smokeNode));

	// Add propellant particle node to the scene
	std::unique_ptr<ParticleNode> propellantNode(new ParticleNode(Particle::Propellant, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(propellantNode));

	// Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// Add network node, if necessary
	if (mNetworkedWorld)
	{
		std::unique_ptr<NetworkNode> networkNode(new NetworkNode());
		mNetworkNode = networkNode.get();
		mSceneGraph.attachChild(std::move(networkNode));
	}

	// Add enemy aircraft
	addEnemies();

	// Add Borders
	addBorders();
}

void World::addEnemies()
{
	if (mNetworkedWorld)
		return;
}

void World::addBorders()
{
	addBorder(Wall::HorizontalWall, 0.f, 375.f);
	addBorder(Wall::HorizontalWall, 0.f, -375.f);
	addBorder(Wall::VerticalWall, -502.f, 0.f);
	addBorder(Wall::VerticalWall, 504.f, 0.f);

	//Ball
	//addBall(Projectile::Ball, 0.f, 0.f);

	//Progress
	addBorder(Wall::AlliedObjectiveProgress, 402.f, 142.f);
	addBorder(Wall::EnemyObjectiveProgress, -400.f, 142.f);

	//Objectives
	addBorder(Wall::AlliedObjective, 402.f, 0.f);
	addBorder(Wall::EnemyObjective, -400.f, 0.f);
}

void World::sortEnemies()
{
	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	std::sort(mEnemySpawnPoints.begin(), mEnemySpawnPoints.end(), [](SpawnPoint lhs, SpawnPoint rhs)
	{
		return lhs.y < rhs.y;
	});
}

void World::addEnemy(Aircraft::Type type, float relX, float relY)
{
	SpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mEnemySpawnPoints.push_back(spawn);
}

void World::addBorder(Wall::Type type, float relX, float relY)
{
	BorderSpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mBorderSpawnPoints.push_back(spawn);
}

void World::addBall(Projectile::Type type, float relX, float relY)
{
	BallSpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mBallSpawnPoints.push_back(spawn);
}

void World::spawnBorders()
{
	int identifier = 1;
	// Spawn all enemies entering the view area (including distance) this frame
	while (!mBorderSpawnPoints.empty()
		&& mBorderSpawnPoints.back().y > getBattlefieldBounds().top)
	{
		int identifier = 1;
		BorderSpawnPoint spawn = mBorderSpawnPoints.back();

		std::unique_ptr<Wall> wall(new Wall(spawn.type, mTextures));


		if (spawn.type == Wall::HorizontalWall)
		{
			wall->setRotation(0.f);
		}
		else
		{
			wall->setRotation(90.f);
		}

		wall->setPosition(spawn.x, spawn.y);
		if (spawn.type == Wall::AlliedObjectiveProgress || spawn.type == Wall::EnemyObjectiveProgress)
		{
			wall->setIdentifier(identifier);
			mProgressBars.push_back(wall.get());
			identifier++;
		}
		mSceneLayers[LowerAir]->attachChild(std::move(wall));

		
		// Enemy is spawned, remove from the list to spawn
		mBorderSpawnPoints.pop_back();
	}

	//spawn ball
	while (!mBallSpawnPoints.empty()
		&& mBallSpawnPoints.back().y > getBattlefieldBounds().top)
	{
		BallSpawnPoint spawn = mBallSpawnPoints.back();

		std::unique_ptr<Projectile> ball(new Projectile(spawn.type, mTextures));

		ball->setPosition(spawn.x, spawn.y);
		ball->setBallValue(0.5);

		if (spawn.type == Projectile::Ball)
		{
			mSceneLayers[UpperAir]->attachChild(std::move(ball));
		}
		mBallSpawnPoints.pop_back();
	}
}


void World::spawnEnemies()
{
	// Spawn all enemies entering the view area (including distance) this frame
	while (!mEnemySpawnPoints.empty()
		&& mEnemySpawnPoints.back().y > getBattlefieldBounds().top)
	{
		SpawnPoint spawn = mEnemySpawnPoints.back();

		std::unique_ptr<Aircraft> enemy(new Aircraft(spawn.type, mTextures, mFonts));
		enemy->setPosition(spawn.x, spawn.y);
		//enemy->setRotation(180.f);
		if (mNetworkedWorld) enemy->disablePickups();

		mSceneLayers[LowerAir]->attachChild(std::move(enemy));

		// Enemy is spawned, remove from the list to spawn
		mEnemySpawnPoints.pop_back();
	}
}

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = Category::Projectile | Category::EnemyAircraft;
	command.action = derivedAction<Entity>([this](Entity& e, sf::Time)
	{
		if (!getBattlefieldBounds().intersects(e.getBoundingRect()))
			e.remove();
	});

	mCommandQueue.push(command);
}

void World::guideMissiles()
{
	// Setup command that stores all enemies in mActiveEnemies
	Command enemyCollector;
	enemyCollector.category = Category::EnemyAircraft;
	enemyCollector.action = derivedAction<Aircraft>([this](Aircraft& enemy, sf::Time)
	{
		if (!enemy.isDestroyed())
			mActiveEnemies.push_back(&enemy);
	});

	// Setup command that guides all missiles to the enemy which is currently closest to the player
	Command missileGuider;
	missileGuider.category = Category::AlliedProjectile;
	missileGuider.action = derivedAction<Projectile>([this](Projectile& missile, sf::Time)
	{
		// Ignore unguided bullets
		if (!missile.isGuided())
			return;

		float minDistance = std::numeric_limits<float>::max();
		Aircraft* closestEnemy = nullptr;

		// Find closest enemy
		FOREACH(Aircraft* enemy, mActiveEnemies)
		{
			float enemyDistance = distance(missile, *enemy);

			if (enemyDistance < minDistance)
			{
				closestEnemy = enemy;
				minDistance = enemyDistance;
			}
		}

		if (closestEnemy)
			missile.guideTowards(closestEnemy->getWorldPosition());
	});

	// Push commands, reset active enemies
	mCommandQueue.push(enemyCollector);
	mCommandQueue.push(missileGuider);
	mActiveEnemies.clear();
}

sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}

float World::getAlliedProgressBarValue()
{
	return mAlliedProgressValue;
}

float World::getEnemyProgressBarValue()
{
	return mEnemyProgressValue;
}
