#ifndef BOOK_CATEGORY_HPP
#define BOOK_CATEGORY_HPP


// Entity/scene node category, used to dispatch commands
namespace Category
{
	enum Type
	{
		None = 0,
		SceneAirLayer = 1 << 0,
		PlayerAircraft = 1 << 1,
		AlliedAircraft = 1 << 2,
		EnemyAircraft = 1 << 3,
		Pickup = 1 << 4,
		AlliedProjectile = 1 << 5,
		EnemyProjectile = 1 << 6,
		ParticleSystem = 1 << 7,
		SoundEffect = 1 << 8,
		Network = 1 << 9,
		WallBorders = 1 << 10,
		Ball = 1 << 11,
		AlliedObjective = 1 << 12,
		EnemyObjective = 1 << 13,
		AlliedObjectiveProgress  = 1 << 14,
		EnemyObjectiveProgress = 1 << 15,

		Aircraft = PlayerAircraft | AlliedAircraft | EnemyAircraft,
		Projectile = AlliedProjectile | EnemyProjectile | Ball,
		Wall = WallBorders | AlliedObjective | EnemyObjective | AlliedObjectiveProgress | EnemyObjectiveProgress
	};
}

#endif // BOOK_CATEGORY_HPP
