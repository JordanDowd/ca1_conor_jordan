#pragma once
#include <SFML/System/Vector2.hpp>

class Controller
{
public:
	Controller(int controllerIndex);

	bool IsConnected();

	// Buttons
	bool A();
	bool B();
	bool X();
	bool Y();
	bool RB();					// Right bumber
	bool LB();					// Left bumper
	bool L3();					// Left thumbstick click
	bool R3();					// Right thumbstick click
	bool Start();
	bool Back();
	bool XboxButton();
	bool LT();					// Left trigger
	bool RT();					// Right trigger
	bool DpadUp();
	bool DpadDown();
	bool DpadLeft();
	bool DpadRight();
	sf::Vector2f LeftThumbstick();
	sf::Vector2f RightThumbstick();
	// Axis & LT / RT & Analog sticks

	float TValue();				// Left and Right Trigger value

	// D-pad & left thumbstick motions


	int GetControllerIndex() const;
	float GetTriggerThreshold() const;
	float GetThumbstickThreshold() const;

	void SetTriggerThreshold(float threshold);
	void SetThumbstickThreshold(float threshold);

private:
	int mControllerIndex;
	float mTriggerThreshold;
	float mThumbstickThreshold;
};