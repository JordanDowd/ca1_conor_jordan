#include "Player.hpp"
#include "CommandQueue.hpp"
#include "Aircraft.hpp"
#include "Foreach.hpp"
#include "NetworkProtocol.hpp"
#include "Constants.hpp"
#include <SFML/Network/Packet.hpp>

#include <map>
#include <string>
#include <algorithm>
#include <iostream>

using namespace std::placeholders;

struct AircraftMover
{
	AircraftMover(float vx, float vy, int identifier)
		: velocity(vx, vy)
		, aircraftID(identifier)
	{
	}

	void operator() (Aircraft& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID)
			aircraft.accelerate(velocity * aircraft.getMaxSpeed());
	}

	sf::Vector2f velocity;
	int aircraftID;
};

struct AircraftFireTrigger
{
	AircraftFireTrigger(int identifier)
		: aircraftID(identifier)
	{
	}

	void operator() (Aircraft& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID)
			aircraft.fire();
	}

	int aircraftID;
};

struct AircraftMissileTrigger
{
	AircraftMissileTrigger(int identifier)
		: aircraftID(identifier)
	{
	}

	void operator() (Aircraft& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID)
			aircraft.launchMissile();
	}

	int aircraftID;
};


Player::Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding)
	: mKeyBinding(binding)
	, mCurrentMissionStatus(MissionRunning)
	, mIdentifier(identifier)
	, mSocket(socket)
	,mJoystick(nullptr)
{

	// If joystick not set and there are available controllers
	if (mJoystick == nullptr && sf::Joystick::Count > 0)
	{
		mJoystickBindingPressed[JoystickButton::RB] = Action::Fire;
		mJoystickBindingPressed[JoystickButton::X] = Action::MoveDown;
		mJoystickBindingPressed[JoystickButton::Square] = Action::MoveLeft;
		mJoystickBindingPressed[JoystickButton::Circle] = Action::MoveRight;
		mJoystickBindingPressed[JoystickButton::Triangle] = Action::MoveUp;
		// Setup controller with local ID
		setJoystick(new Controller(identifier));
	}
	else
	{
		// Else, reset the joystick if disconnected
		setJoystick(nullptr);
	}
	
	// Set initial action bindings
	initializeActions();

	// Assign all categories to player's aircraft
	FOREACH(auto& pair, mActionBinding)
		pair.second.category = Category::PlayerAircraft;
}

void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{

	
	if (mJoystick != nullptr)
	{
		if (event.type == sf::Event::JoystickButtonPressed)
		{



			auto found = mJoystickBindingPressed.find(static_cast<JoystickButton>(event.joystickButton.button));

			if (found != mJoystickBindingPressed.end() && !isRealtimeAction(found->second))
			{
				commands.push(mActionBinding[found->second]);
			}

		}
	}
	// Event
	if (event.type == sf::Event::KeyPressed)
	{
		Action action;
		if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && !isRealtimeAction(action))
		{
			// Network connected -> send event over network
			if (mSocket)
			{
				sf::Packet packet;
				packet << static_cast<sf::Int32>(Client::PlayerEvent);
				packet << mIdentifier;
				packet << static_cast<sf::Int32>(action);
				mSocket->send(packet);
			}

			// Network disconnected -> local event
			else
			{
				commands.push(mActionBinding[action]);
			}
		}
	}

	// Realtime change (network connected)
	if ((event.type == sf::Event::KeyPressed || event.type == sf::Event::KeyReleased) && mSocket)
	{
		Action action;
		if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && isRealtimeAction(action))
		{
			// Send realtime change over network
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
			packet << mIdentifier;
			packet << static_cast<sf::Int32>(action);
			packet << (event.type == sf::Event::KeyPressed);
			mSocket->send(packet);
		}
	}
}

bool Player::isLocal() const
{
	// No key binding means this player is remote
	return mKeyBinding != nullptr;
}

sf::TcpSocket * Player::getSocket()
{
	return mSocket;
}

void Player::disableAllRealtimeActions()
{
	FOREACH(auto& action, mActionProxies)
	{
		sf::Packet packet;
		packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
		packet << mIdentifier;
		packet << static_cast<sf::Int32>(action.first);
		packet << false;
		mSocket->send(packet);
	}
}

void Player::handleRealtimeInput(CommandQueue& commands)
{

	//// Check if this is a networked game and local player or just a single player game
	if ((mSocket && isLocal()) || !mSocket)
		{

			if (mJoystick != nullptr)
			{
				for (auto pair : mJoystickBindingPressed)
				{
					if (sf::Joystick::isButtonPressed(mLocalIdentifier, static_cast<unsigned int>(pair.first)) && isRealtimeAction(pair.second))
					{
						commands.push(mActionBinding[pair.second]);
					}
				}


				if (sf::Event::JoystickMoved)
				{

					sf::Joystick::update();
					for (int j = 0; j < sf::Joystick::Count; j++) {
						if (!sf::Joystick::isConnected(j))
							continue;
						//Axes
						if (sf::Joystick::hasAxis(j, sf::Joystick::X)) {
							if (sf::Joystick::getAxisPosition(j, sf::Joystick::X) > ANALOG_THRESHOLD)
							{
								commands.push(mActionBinding[Action::MoveRight]);
							}
							if (sf::Joystick::getAxisPosition(j, sf::Joystick::X) < -ANALOG_THRESHOLD)
							{
								commands.push(mActionBinding[Action::MoveLeft]);
							}
						}
						if (sf::Joystick::hasAxis(j, sf::Joystick::Y)) {
							if (sf::Joystick::getAxisPosition(j, sf::Joystick::Y) > ANALOG_THRESHOLD)
							{
								commands.push(mActionBinding[Action::MoveDown]);
							}
							if (sf::Joystick::getAxisPosition(j, sf::Joystick::Y) < -ANALOG_THRESHOLD)
							{
								commands.push(mActionBinding[Action::MoveUp]);
							}
						}

					}
				}
			}
		// Lookup all actions and push corresponding commands to queue
		std::vector<Action> activeActions = mKeyBinding->getRealtimeActions();
	FOREACH(Action action, activeActions)
		commands.push(mActionBinding[action]);
}
}

void Player::handleRealtimeNetworkInput(CommandQueue& commands)
{

	if (mSocket && !isLocal())
	{
		// Traverse all realtime input proxies. Because this is a networked game, the input isn't handled directly
		FOREACH(auto pair, mActionProxies)
		{
			if (pair.second && isRealtimeAction(pair.first))
				commands.push(mActionBinding[pair.first]);
		}
	}
}

void Player::handleNetworkEvent(Action action, CommandQueue& commands)
{
	commands.push(mActionBinding[action]);
}

void Player::handleNetworkRealtimeChange(Action action, bool actionEnabled)
{
	mActionProxies[action] = actionEnabled;
}

void Player::setMissionStatus(MissionStatus status)
{
	mCurrentMissionStatus = status;
}

Player::MissionStatus Player::getMissionStatus() const
{
	return mCurrentMissionStatus;
}

void Player::setJoystick(Controller * joystick)
{
	mJoystick = joystick;
}

void Player::initializeActions()
{
	mActionBinding[PlayerAction::MoveLeft].action = derivedAction<Aircraft>(AircraftMover(-1, 0, mIdentifier));
	mActionBinding[PlayerAction::MoveRight].action = derivedAction<Aircraft>(AircraftMover(+1, 0, mIdentifier));
	mActionBinding[PlayerAction::MoveUp].action = derivedAction<Aircraft>(AircraftMover(0, -1, mIdentifier));
	mActionBinding[PlayerAction::MoveDown].action = derivedAction<Aircraft>(AircraftMover(0, +1, mIdentifier));
	mActionBinding[PlayerAction::Fire].action = derivedAction<Aircraft>(AircraftFireTrigger(mIdentifier));
	//mActionBinding[PlayerAction::LaunchMissile].action = derivedAction<Aircraft>(AircraftMissileTrigger(mIdentifier));
}
