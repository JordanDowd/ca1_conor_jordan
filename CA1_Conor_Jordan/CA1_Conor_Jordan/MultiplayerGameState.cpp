#include "MultiplayerGameState.hpp"
#include "MusicPlayer.hpp"
#include "Foreach.hpp"
#include "Utility.hpp"
#include "Constants.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Network/IpAddress.hpp>

#include <fstream>
#include <iostream>
#include <cmath>

sf::IpAddress getAddressFromFile()
{
	{ // Try to open existing file (RAII block)
		std::ifstream inputFile("ip.txt");
		std::string ipAddress;
		if (inputFile >> ipAddress)
			return ipAddress;
	}

	// If open/read failed, create new file
	std::ofstream outputFile("ip.txt");
	std::string localAddress = "127.0.0.1";
	outputFile << localAddress;
	return localAddress;
}

MultiplayerGameState::MultiplayerGameState(StateStack& stack, Context context, bool isHost)
	: State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds, *context.music, true)
	, mWindow(*context.window)
	, mTextureHolder(*context.textures)
	, mConnected(false)
	, mGameServer(nullptr)
	, mActiveState(false)
	, mSounds(*context.sounds)
	, mMusic(*context.music)
	, mHasFocus(true)
	, mHost(isHost)
	, mGameStarted(false)
	, mBeginCountdown(false)
	, mCountdownTimer(2)
	, mClientTimeout(sf::seconds(2.f))
	, mTimeSinceLastPacket(sf::seconds(0.f))
	, mBallSpawned(false)
	, mAlliedProgressBarValue(0)
	, mEnemyProgressBarValue(0)
{
	sf::Vector2f windowSize(context.window->getSize());

	mBroadcastText.setFont(context.fonts->get(Fonts::Main));
	mBroadcastText.setPosition(1024.f / 2, 100.f);

	mCountdownText.setFont(context.fonts->get(Fonts::Main));
	mCountdownText.setPosition(0.5f * windowSize.x, 0.5f * windowSize.y);
	mCountdownText.setCharacterSize(150u);

	mLobbyText.setFont(context.fonts->get(Fonts::Main));
	mLobbyText.setString("Waiting for host to start the game");
	centerOrigin(mLobbyText);
	mLobbyText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);

	mHostText.setFont(context.fonts->get(Fonts::Main));
	if (mHost)
		mHostText.setString("You are the Host");
	else
		mHostText.setString("You are not the host");
	mHostText.setCharacterSize(18);
	centerOrigin(mHostText);
	mHostText.setPosition(0.5f * windowSize.x, 0.3f * windowSize.y);

	mHostStartText.setFont(context.fonts->get(Fonts::Main));
	mHostStartText.setString("Press Enter when ready");
	mHostStartText.setCharacterSize(18);
	centerOrigin(mHostStartText);
	mHostStartText.setPosition(0.5f * windowSize.x, 0.5f * windowSize.y);

	mPlayersConnectedText.setFont(context.fonts->get(Fonts::Main));
	mPlayersConnectedText.setCharacterSize(22);
	mPlayersConnectedText.setPosition(0.5f * windowSize.x, 0.7f * windowSize.y);

	// We reuse this text for "Attempt to connect" and "Failed to connect" messages
	mFailedConnectionText.setFont(context.fonts->get(Fonts::Main));
	mFailedConnectionText.setString("Attempting to connect...");
	mFailedConnectionText.setCharacterSize(35);
	mFailedConnectionText.setFillColor(sf::Color::White);
	centerOrigin(mFailedConnectionText);
	mFailedConnectionText.setPosition(mWindow.getSize().x / 2.f, mWindow.getSize().y / 2.f);

	// Render a "establishing connection" frame for user feedback
	mWindow.clear(sf::Color::Black);
	mWindow.draw(mFailedConnectionText);
	mWindow.display();
	mFailedConnectionText.setString("Could not connect to the remote server!");
	centerOrigin(mFailedConnectionText);

	mGreenProgressValueText.setFont(context.fonts->get(Fonts::Main));
	mGreenProgressValueText.setPosition(910.f, mWindow.getSize().y / 2.f);
	mGreenProgressValueText.setFillColor(sf::Color(0, 0, 0, 105));

	mRedProgressValueText.setFont(context.fonts->get(Fonts::Main));
	mRedProgressValueText.setPosition(110.f, mWindow.getSize().y / 2.f);
	mRedProgressValueText.setFillColor(sf::Color(0, 0, 0, 105));

	sf::IpAddress ip;
	if (isHost)
	{
		mGameServer.reset(new GameServer(sf::Vector2f(mWindow.getSize())));
		ip = "127.0.0.1";
	}
	else
	{
		ip = getAddressFromFile();
	}

	if (mSocket.connect(ip, ServerPort, sf::seconds(5.f)) == sf::TcpSocket::Done)
	{
		mConnected = true;
	}
	else
		mFailedConnectionClock.restart();

	mSocket.setBlocking(false);

	// Play game theme
	context.music->stop();
}

void MultiplayerGameState::draw()
{
	if (mConnected)
	{
		mWorld.draw();

		// Broadcast messages in default view
		mWindow.setView(mWindow.getDefaultView());

		if (!mBroadcasts.empty())
			mWindow.draw(mBroadcastText);

		//if (mLocalPlayerIdentifiers.size() < 2 && mPlayerInvitationTime < sf::seconds(0.5f))
			//mWindow.draw(mPlayerInvitationText);

		mWindow.draw(mGreenProgressValueText);
		mWindow.draw(mRedProgressValueText);

		//Lobby screen
		if (!mGameStarted)
		{
			if (mHost)
			{
				mActiveState = true;
			}

			sf::RenderWindow& window = *getContext().window;
			window.setView(window.getDefaultView());

			mLobbyScreen.setFillColor(sf::Color(0, 0, 0, 255));
			mLobbyScreen.setSize(window.getView().getSize());

			window.draw(mLobbyScreen);
			window.draw(mHostText);

			mPlayersConnectedText.setString(std::to_string(mPlayers.size()) + "  players connected");
			centerOrigin(mPlayersConnectedText);
			window.draw(mPlayersConnectedText);

			if (mLocalPlayerIdentifiers.size() < 2 && mLobbyTime < sf::seconds(0.8f))
				window.draw(mLobbyText);

			if (mHost)
				window.draw(mHostStartText);
		}

		if (mBeginCountdown && mGameStarted)
		{
			sf::RenderWindow& window = *getContext().window;
			window.setView(window.getDefaultView());

			mCountdownScreen.setFillColor(mCountdownScreenAlpha);
			mCountdownScreen.setSize(window.getView().getSize());

			window.draw(mCountdownScreen);
			window.draw(mCountdownText);
		}
	}
	else
	{
		mWindow.draw(mFailedConnectionText);
	}
}

void MultiplayerGameState::onActivate()
{
	mActiveState = true;
}

void MultiplayerGameState::onDestroy()
{
	if (!mHost && mConnected)
	{
		// Inform server this client is dying
		sf::Packet packet;
		packet << static_cast<sf::Int32>(Client::Quit);
		mSocket.send(packet);
	}
}

bool MultiplayerGameState::update(sf::Time dt)
{
	// Connected to server: Handle all the network logic
	if (mConnected)
	{
		mWorld.update(dt);

		// Remove players whose aircrafts were destroyed
		bool foundLocalPlane = false;
		for (auto itr = mPlayers.begin(); itr != mPlayers.end(); )
		{
			// Check if there are no more local planes for remote clients
			if (std::find(mLocalPlayerIdentifiers.begin(), mLocalPlayerIdentifiers.end(), itr->first) != mLocalPlayerIdentifiers.end())
			{
				foundLocalPlane = true;
			}

			if (!mWorld.getAircraft(itr->first))
			{
				itr = mPlayers.erase(itr);

				// No more players left: Mission failed
				if (mPlayers.empty())
					requestStackPush(States::GameOver);
			}
			else
			{
				++itr;
			}
		}

		if (!foundLocalPlane && mGameStarted)
		{
			requestStackPush(States::GameOver);
		}

		// Only handle the realtime input if the window has focus and the game is unpaused
		if (mActiveState && mHasFocus)
		{
			CommandQueue& commands = mWorld.getCommandQueue();
			FOREACH(auto& pair, mPlayers)
				pair.second->handleRealtimeInput(commands);
		}

		// Always handle the network input
		CommandQueue& commands = mWorld.getCommandQueue();
		FOREACH(auto& pair, mPlayers)
			pair.second->handleRealtimeNetworkInput(commands);

		// Handle messages from server that may have arrived
		sf::Packet packet;
		if (mSocket.receive(packet) == sf::Socket::Done)
		{
			mTimeSinceLastPacket = sf::seconds(0.f);
			sf::Int32 packetType;
			packet >> packetType;
			handlePacket(packetType, packet);
		}
		else
		{
			// Check for timeout with the server
			if (mTimeSinceLastPacket > mClientTimeout)
			{
				mConnected = false;

				mFailedConnectionText.setString("Lost connection to server");
				centerOrigin(mFailedConnectionText);

				mFailedConnectionClock.restart();
			}
		}

		updateBroadcastMessage(dt);

		// Time counter for blinking 2nd player text
		mPlayerInvitationTime += dt;
		if (mPlayerInvitationTime > sf::seconds(1.f))
			mPlayerInvitationTime = sf::Time::Zero;

		// Time counter for blinking 2nd player text
		mLobbyTime += dt;
		if (mLobbyTime > sf::seconds(1.f))
			mLobbyTime = sf::Time::Zero;

		// Events occurring in the game
		GameActions::Action gameAction;
		while (mWorld.pollGameAction(gameAction))
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::GameEvent);
			packet << static_cast<sf::Int32>(gameAction.type);
			packet << gameAction.position.x;
			packet << gameAction.position.y;

			mSocket.send(packet);
		}

		if (/*mHost &&*/ mWorld.playerBallCollisionOccured())
		{
			std::cout << "Knows collision occured" << std::endl;
			mWorld.setPlayerBallCollisionOccured(false, mWorld.getPlayerHolding());
			
			sf::Int32 playerID = mWorld.getPlayerHolding()->getIdentifier();
							std::cout << "trying to send" << std::endl;
							sf::Packet updatePlayerCaughtPacket;
							updatePlayerCaughtPacket << static_cast<sf::Int32>(Client::UpdateBallCaught);
							updatePlayerCaughtPacket << playerID;
							mSocket.send(updatePlayerCaughtPacket);


			//FOREACH(sf::Int32 identifier, mLocalPlayerIdentifiers)
			//{
			//	if (Aircraft* aircraft = mWorld.getAircraft(identifier))
			//	{
			//		std::cout << mWorld.getAircraft(identifier)->isBallCaught() << std::endl;
			//			if (mWorld.getAircraft(identifier)->isBallCaught())
			//			{
			//				sf::Int32 ballID = mWorld.getAircraft(identifier)->getBall()->getIdentifier();
			//				std::cout << "trying to send" << std::endl;
			//				sf::Packet updatePlayerCaughtPacket;
			//				updatePlayerCaughtPacket << static_cast<sf::Int32>(Client::UpdateBallCaught);
			//			/*	updatePlayerCaughtPacket << ballID;*/
			//				updatePlayerCaughtPacket << identifier;

			//				mSocket.send(updatePlayerCaughtPacket);
			//			}
			//	}
			//}
		}

		// Regular updates to progress bar values, host controls these values only
		if (mHost)
		{
			if (mTickClock.getElapsedTime() > sf::seconds(1.f / 20.f))
			{
				sf::Packet progressBarUpdatePacket;
				float alliedProgress = mWorld.getAlliedProgressBarValue();
				float enemyProgress = mWorld.getEnemyProgressBarValue();
				progressBarUpdatePacket << static_cast<sf::Int32>(Client::ProgressBarUpdate);
				progressBarUpdatePacket << alliedProgress;
				progressBarUpdatePacket << enemyProgress;
				mSocket.send(progressBarUpdatePacket);
			}
		}


		// Regular position updates
		if (mTickClock.getElapsedTime() > sf::seconds(1.f / 20.f))
		{
			sf::Packet positionUpdatePacket;
			positionUpdatePacket << static_cast<sf::Int32>(Client::PositionUpdate);
			positionUpdatePacket << static_cast<sf::Int32>(mLocalPlayerIdentifiers.size());


			FOREACH(sf::Int32 identifier, mLocalPlayerIdentifiers)
			{
				if (Aircraft* aircraft = mWorld.getAircraft(identifier))
				{
					positionUpdatePacket << identifier << aircraft->getPosition().x << aircraft->getPosition().y;
					/*
					if (mWorld.getAircraft(identifier)->isBallCaught())
					{
						sf::Int32 ballID = mWorld.getAircraft(identifier)->getBall()->getIdentifier();
						std::cout << "trying to send" << std::endl;
						sf::Packet updatePlayerCaughtPacket;
						updatePlayerCaughtPacket << static_cast<sf::Int32>(Client::UpdateBallCaught);
						updatePlayerCaughtPacket << ballID;
						updatePlayerCaughtPacket << identifier;

						mSocket.send(updatePlayerCaughtPacket);
					}*/
				}
			}

			mSocket.send(positionUpdatePacket);

			//holds list of balls and their position
			sf::Packet ballPositionUpdatePacket;

			ballPositionUpdatePacket << static_cast<sf::Int32>(Client::BallPositionUpdate);

			ballPositionUpdatePacket << static_cast<sf::Int32>(mLocalBallIdentifiers.size());
			FOREACH(sf::Int32 identifier, mLocalBallIdentifiers)
			{
				if (Projectile* ball = mWorld.GetBall(identifier))
				{
					ballPositionUpdatePacket << identifier << ball->getPosition().x << ball->getPosition().y;
				}

			}
			mSocket.send(ballPositionUpdatePacket);

			mTickClock.restart();
		}

		mTimeSinceLastPacket += dt;
	}

	// Failed to connect and waited for more than 5 seconds: Back to menu
	else if (mFailedConnectionClock.getElapsedTime() >= sf::seconds(5.f))
	{
		requestStateClear();
		requestStackPush(States::Menu);
	}

	// Failed to connect and waited for more than 5 seconds: Back to menu
	if (mCountdownClock.getElapsedTime() >= sf::seconds(1.f) && mBeginCountdown)
	{
		if (mCountdownTimer > COUNTDOWN_TIMER_END)
		{
			mSounds.play(SoundEffect::CountdownTimer);
		}

		if (mCountdownTimer <= COUNTDOWN_TIMER_END)
		{
			if (mCountdownTimer > COUNTDOWN_FINISH_SCORE_STOP_TIME)
			{
				mSounds.play(SoundEffect::CountdownFinish);
				mCountdownTimer--;
			}
			mCountdownText.setString("GO!");
			centerOrigin(mCountdownText);
			mActiveState = true;

			if (mCountdownClock.getElapsedTime() >= sf::seconds(2.f))
			{
				mBeginCountdown = false;
			}
		}
		else
		{
			mCountdownText.setString(std::to_string(mCountdownTimer));
			centerOrigin(mCountdownText);
			mCountdownClock.restart();
			mCountdownTimer = mCountdownTimer - 1;
		}

	}

	if (mBeginCountdown)
	{
		mCountdownScreenAlpha = mCountdownScreenAlpha - sf::Color(0, 0, 0, 1);
	}

	return true;
}

void MultiplayerGameState::disableAllRealtimeActions()
{
	mActiveState = false;

	FOREACH(sf::Int32 identifier, mLocalPlayerIdentifiers)
		mPlayers[identifier]->disableAllRealtimeActions();
}

bool MultiplayerGameState::handleEvent(const sf::Event& event)
{
	// Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();

	// Forward event to all players
	FOREACH(auto& pair, mPlayers)
		pair.second->handleEvent(event, commands);

	if (event.type == sf::Event::KeyPressed)
	{
		// Enter pressed, add second player co-op (only if we are one player)
		if (event.key.code == sf::Keyboard::Return)
		{
			if (!mGameStarted && mHost && !mBallSpawned)
			{
				mBallSpawned = true;
				sf::Packet packet;
				packet << static_cast<sf::Int32>(Client::RequestSpawnBall);

				//send a packet to inform the sever to start the game
				sf::Packet startGamePacket;
				startGamePacket << static_cast<sf::Int32>(Client::StartGame);

				mSocket.send(packet);
				mSocket.send(startGamePacket);
			}
		}

		// Escape pressed, trigger the pause screen
		else if (event.key.code == sf::Keyboard::Escape)
		{
			disableAllRealtimeActions();
			requestStackPush(States::NetworkPause);
		}
		else if (event.key.code == sf::Keyboard::R)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::ResetBalls);

			mSocket.send(packet);
		}
	}
	else if (event.type == sf::Event::GainedFocus)
	{
		mHasFocus = true;
	}
	else if (event.type == sf::Event::LostFocus)
	{
		mHasFocus = false;
	}

	return true;
}

void MultiplayerGameState::updateBroadcastMessage(sf::Time elapsedTime)
{
	if (mBroadcasts.empty())
		return;

	// Update broadcast timer
	mBroadcastElapsedTime += elapsedTime;
	if (mBroadcastElapsedTime > sf::seconds(2.5f))
	{
		// If message has expired, remove it
		mBroadcasts.erase(mBroadcasts.begin());

		// Continue to display next broadcast message
		if (!mBroadcasts.empty())
		{
			mBroadcastText.setString(mBroadcasts.front());
			centerOrigin(mBroadcastText);
			mBroadcastElapsedTime = sf::Time::Zero;
		}
	}
}

void MultiplayerGameState::handlePacket(sf::Int32 packetType, sf::Packet& packet)
{
	switch (packetType)
	{
		// Send message to all clients
	case Server::BroadcastMessage:
	{
		std::string message;
		packet >> message;
		mBroadcasts.push_back(message);

		// Just added first message, display immediately
		if (mBroadcasts.size() == 1)
		{
			mBroadcastText.setString(mBroadcasts.front());
			centerOrigin(mBroadcastText);
			mBroadcastElapsedTime = sf::Time::Zero;
		}
	} break;

	// Sent by the server to order to spawn player 1 airplane on connect
	case Server::SpawnSelf:
	{
		sf::Int32 aircraftIdentifier;
		sf::Vector2f aircraftPosition;
		packet >> aircraftIdentifier >> aircraftPosition.x >> aircraftPosition.y;

		Aircraft* aircraft = mWorld.addAircraft(aircraftIdentifier);
		aircraft->setPosition(aircraftPosition);
		aircraft->updateTexture();

		mPlayers[aircraftIdentifier].reset(new Player(&mSocket, aircraftIdentifier, getContext().keys1));
		mLocalPlayerIdentifiers.push_back(aircraftIdentifier);

		//mGameStarted = true;
	} break;

	// 
	case Server::PlayerConnect:
	{
		sf::Int32 aircraftIdentifier;
		sf::Vector2f aircraftPosition;
		packet >> aircraftIdentifier >> aircraftPosition.x >> aircraftPosition.y;

		Aircraft* aircraft = mWorld.addAircraft(aircraftIdentifier);
		aircraft->setPosition(aircraftPosition);

		mPlayers[aircraftIdentifier].reset(new Player(&mSocket, aircraftIdentifier, nullptr));
	} break;

	case Server::BallConnect:
	{
		sf::Int32 ballIdentifier;
		sf::Vector2f ballPosition;
		packet >> ballIdentifier >> ballPosition.x >> ballPosition.y;

		Projectile* ball = mWorld.addBall(ballIdentifier);
		ball->setPosition(ballPosition);

		//mPlayers[aircraftIdentifier].reset(new Player(&mSocket, aircraftIdentifier, nullptr));
	}break;

	case Server::AcceptResetBalls:
	{
		Projectile* ball = mWorld.GetBall(0);
		ball->getPlayerCaught()->setIsFiring(true);
		ball->getPlayerCaught()->setBallCanMove(true);
		ball->setPlayerCaught(false);
	}break;
	// 
	case Server::PlayerDisconnect:
	{
		sf::Int32 aircraftIdentifier;
		packet >> aircraftIdentifier;

		mWorld.removeAircraft(aircraftIdentifier);
		mPlayers.erase(aircraftIdentifier);
	} break;

	// 
	case Server::InitialState:
	{
		sf::Int32 aircraftCount;
		sf::Int32 ballCount;
		float worldHeight, currentScroll;
		packet >> worldHeight >> currentScroll;

		mWorld.setWorldHeight(worldHeight);
		mWorld.setCurrentBattleFieldPosition(currentScroll);

		packet >> aircraftCount;
		for (sf::Int32 i = 0; i < aircraftCount; ++i)
		{
			sf::Int32 aircraftIdentifier;
			sf::Vector2f aircraftPosition;
			packet >> aircraftIdentifier >> aircraftPosition.x >> aircraftPosition.y;

			Aircraft* aircraft = mWorld.addAircraft(aircraftIdentifier);
			aircraft->setPosition(aircraftPosition);

			mPlayers[aircraftIdentifier].reset(new Player(&mSocket, aircraftIdentifier, nullptr));
		}


		packet >> ballCount;
		for (sf::Int32 i = 0; i < ballCount; ++i)
		{
			sf::Int32 ballIdentifier;
			sf::Vector2f ballPosition;
			packet >> ballIdentifier >> ballPosition.x >> ballPosition.y;

			Projectile* ball = mWorld.addBall(ballIdentifier);
			ball->setPosition(ballPosition);
			//mBalls[ballIdentifier].reset(new Player(&mSocket, aircraftIdentifier, nullptr));
		}
	} break;

	//
	case Server::AcceptSpawnBall:
	{
		sf::Int32 ballIdentifier;
		packet >> ballIdentifier;

		mWorld.addBall(ballIdentifier);
		mLocalBallIdentifiers.push_back(ballIdentifier);
	} break;

	// Player event (like missile fired) occurs
	case Server::PlayerEvent:
	{
		sf::Int32 aircraftIdentifier;
		sf::Int32 action;
		packet >> aircraftIdentifier >> action;

		auto itr = mPlayers.find(aircraftIdentifier);
		if (itr != mPlayers.end())
			itr->second->handleNetworkEvent(static_cast<Player::Action>(action), mWorld.getCommandQueue());
	} break;

	// Player's movement or fire keyboard state changes
	case Server::PlayerRealtimeChange:
	{
		sf::Int32 aircraftIdentifier;
		sf::Int32 action;
		bool actionEnabled;
		packet >> aircraftIdentifier >> action >> actionEnabled;

		auto itr = mPlayers.find(aircraftIdentifier);
		if (itr != mPlayers.end())
			itr->second->handleNetworkRealtimeChange(static_cast<Player::Action>(action), actionEnabled);
	} break;

	// New enemy to be created
	case Server::SpawnEnemy:
	{
		float height;
		sf::Int32 type;
		float relativeX;
		packet >> type >> height >> relativeX;

		mWorld.addEnemy(static_cast<Aircraft::Type>(type), relativeX, height);
		mWorld.sortEnemies();
	} break;

	// Mission successfully completed
	case Server::MissionSuccess:
	{
		float alliedProgress = mWorld.getAlliedProgressBarValue();
		float enemyProgress = mWorld.getEnemyProgressBarValue();

		mSounds.play(SoundEffect::FinishSoundOne);

		if (alliedProgress > enemyProgress)
		{
			//mSounds.play(SoundEffect::FinishSoundOne);
			requestStackPush(States::RedTeamWins);
		}
		else
		{
			requestStackPush(States::GreenTeamWins);
			//mSounds.play(SoundEffect::FinishSoundTwo);
		}
	} break;

	// Pickup created
	case Server::SpawnPickup:
	{
		sf::Int32 type;
		sf::Vector2f position;
		packet >> type >> position.x >> position.y;

		mWorld.createPickup(position, static_cast<Pickup::Type>(type));
	} break;

	//Start Game
	case Server::StartGame:
	{
		mGameStarted = true;
		mBeginCountdown = true;
		mCountdownClock.restart();
		//std::cout << mGameStarted << std::endl;
	} break;

	//Progress Bar Update
	case Server::ProgressBarUpdate:
	{
		packet >> mAlliedProgressBarValue;
		packet >> mEnemyProgressBarValue;

		Wall* alliedProgressBar = mWorld.getProgressBar(Wall::AlliedObjectiveProgress);
		Wall* enemyProgressBar = mWorld.getProgressBar(Wall::EnemyObjectiveProgress);

		enemyProgressBar->updateTexture(mEnemyProgressBarValue);
		alliedProgressBar->updateTexture(mAlliedProgressBarValue);

		mGreenBarValue = round((mAlliedProgressBarValue / 285) * 100);
		mRedBarValue = round((mEnemyProgressBarValue / 285) * 100);
		//draw percentage of each progress bar on screen
		mGreenProgressValueText.setString(std::to_string(mGreenBarValue));
		mRedProgressValueText.setString(std::to_string(mRedBarValue));

		centerOrigin(mGreenProgressValueText);
		centerOrigin(mRedProgressValueText);


	} break;

	//
	case Server::UpdateClientState:
	{

		sf::Int32 aircraftCount;
		packet >> aircraftCount;

		for (sf::Int32 i = 0; i < aircraftCount; ++i)
		{
			sf::Vector2f aircraftPosition;
			sf::Int32 aircraftIdentifier;
			packet >> aircraftIdentifier >> aircraftPosition.x >> aircraftPosition.y;

			Aircraft* aircraft = mWorld.getAircraft(aircraftIdentifier);
			bool isLocalPlane = std::find(mLocalPlayerIdentifiers.begin(), mLocalPlayerIdentifiers.end(), aircraftIdentifier) != mLocalPlayerIdentifiers.end();
			if (aircraft && !isLocalPlane)
			{
				sf::Vector2f interpolatedPosition = aircraft->getPosition() + (aircraftPosition - aircraft->getPosition()) * 0.1f;
				aircraft->setPosition(interpolatedPosition);
			}
		}

		sf::Int32 ballCount;
		packet >> ballCount;

		for (sf::Int32 i = 0; i < ballCount; ++i)
		{
			sf::Vector2f ballPosition;
			sf::Int32 ballIdentifier;
			//sf::Int32 playerID;
			//bool isCaught;
			packet >> ballIdentifier >> ballPosition.x >> ballPosition.y;
			Projectile* ball = mWorld.GetBall(ballIdentifier);
			bool isLocalPlane = std::find(mLocalBallIdentifiers.begin(), mLocalBallIdentifiers.end(), ballIdentifier) != mLocalBallIdentifiers.end();
			if (ball && !isLocalPlane)
			{
				sf::Vector2f interpolatedPosition = ball->getPosition() + (ballPosition - ball->getPosition()) * 0.1f;
				ball->setPosition(interpolatedPosition);
			}
		}

	} break;

	case Server::UpdateBallCaughtAccepted:
	{
		std::cout << "updating Ball caught" << std::endl;
		sf::Int32 ballIdentifier;
		sf::Int32 playerID;
		packet >> /*ballIdentifier >> */playerID;
		Projectile* ball = mWorld.GetBall(0);
		mWorld.getAircraft(playerID)->setIsBallCaught(ball, true);
		mWorld.getAircraft(playerID)->ballCollidedWithPlayer();
	} break;
	}
}
