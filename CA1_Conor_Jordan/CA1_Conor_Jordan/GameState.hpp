#ifndef BOOK_GAMESTATE_HPP
#define BOOK_GAMESTATE_HPP

#include "State.hpp"
#include "World.hpp"
#include "Player.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GameState : public State
{
	public:
							GameState(StateStack& stack, Context context);

		virtual void		draw();
		virtual bool		update(sf::Time dt);
		virtual bool		handleEvent(const sf::Event& event);

private:
	typedef std::unique_ptr<Player> PlayerPtr;

	private:
		World				mWorld;
		Player				mPlayer1;
		Player				mPlayer2;
		std::map<int, PlayerPtr>	mPlayers;
		std::vector<sf::Int32>		mLocalPlayerIdentifiers;
};

#endif // BOOK_GAMESTATE_HPP