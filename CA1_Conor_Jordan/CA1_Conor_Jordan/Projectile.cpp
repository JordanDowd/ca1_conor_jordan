#include "Constants.hpp"
#include "Projectile.hpp"
#include "EmitterNode.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
#include "SoundNode.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include "CommandQueue.hpp"
#include <cmath>
#include <cassert>
#include <cstdlib>

namespace
{
	const std::vector<ProjectileData> Table = initializeProjectileData();
}

Projectile::Projectile(Type type, const TextureHolder& textures)
: Entity(1)
, mType(type)
, mSprite(textures.get(Table[type].texture), Table[type].textureRect)
, mTargetDirection()
, mBallIsCaught(false)
, mBallValue(0.5f)
{
	centerOrigin(mSprite);

	// Add particle system for missiles
		std::unique_ptr<EmitterNode> smoke(new EmitterNode(Particle::Smoke));
		smoke->setPosition(0.f, 0.f);
		attachChild(std::move(smoke));

		std::unique_ptr<EmitterNode> propellant(new EmitterNode(Particle::Propellant));
		propellant->setPosition(0.f, 0.f);
		attachChild(std::move(propellant));
}

void Projectile::guideTowards(sf::Vector2f position)
{
	assert(isGuided());
	mTargetDirection = unitVector(position - getWorldPosition());
}

bool Projectile::isGuided() const
{
	return mType == Missile;
}

int Projectile::getIdentifier()
{
	return mIdentifier;
}

void Projectile::setIdentifier(int identifier)
{
	mIdentifier = identifier;
}

void Projectile::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	if (!mBallIsCaught)
	{

		setVelocity(getVelocity().x, getVelocity().y);
		if (abs(getVelocity().x) >= SLOWEST_BALL_SPEED)
		{
			if (getVelocity().x < 0)
			{
				accelerate(SLOW_DOWN_AMOUNT, 0);
			}
			else
			{
				accelerate(-SLOW_DOWN_AMOUNT, 0);
			}
		}
		if (abs(getVelocity().y) >= SLOWEST_BALL_SPEED)
		{
			if (getVelocity().y < 0)
			{
				accelerate(0, SLOW_DOWN_AMOUNT);
			}
			else
			{
				accelerate(0, -SLOW_DOWN_AMOUNT);
			}

		}
	}
	else
	{
		if(mPlayer->ballCanMove() == false)
		setPosition(mPlayer->getWorldPosition());
	}
	if (mHitWall)
	{
		mHitWall = false;
		playLocalSound(commands, SoundEffect::BounceBall);
	}

	Entity::updateCurrent(dt, commands);
}

void Projectile::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}

unsigned int Projectile::getCategory() const
{
	if (mType == EnemyBullet)
		return Category::EnemyProjectile;
	else
		return Category::AlliedProjectile;
}

sf::FloatRect Projectile::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

float Projectile::getMaxSpeed() const
{
	return Table[mType].speed;
}

int Projectile::getDamage() const
{
	return Table[mType].damage;
}

void Projectile::setPlayerCaught(Aircraft * player, bool caught)
{
	if (mPlayer != NULL)
	{
		if (mPlayer->getIdentifier() != player->getIdentifier())
		{
			mPlayer->setIntercepted(true);
			mPlayer->setBallCanMove(true);
			mPlayer->setIsFiring(true);
			mPlayer->setIsBallCaught(false);
		}
	}
	mBallIsCaught = caught;
	mPlayer = player;
	mPlayer->setIsBallCaught(true);
}

Aircraft * Projectile::getPlayerCaught()
{
	if(mPlayer!=NULL)
	return mPlayer;
}

bool Projectile::isBallCaught()
{
	return mBallIsCaught;
}

void Projectile::setPlayerCaught(bool caught)
{
	mBallIsCaught = caught;
	mPlayer->setIsBallCaught(false);
}

float Projectile::getBallValue() const
{
	return mBallValue;
}

void Projectile::setBallValue(float value)
{
	mBallValue = value;
}

unsigned int Projectile::getType() const
{
		return mType;
}

void Projectile::playLocalSound(CommandQueue& commands, SoundEffect::ID effect)
{
	sf::Vector2f worldPosition = getWorldPosition();

	Command command;
	command.category = Category::SoundEffect;
	command.action = derivedAction<SoundNode>(
		[effect, worldPosition](SoundNode& node, sf::Time)
	{
		node.playSound(effect, worldPosition);
	});

	commands.push(command);
}

void Projectile::setHitWall(bool hit)
{
	mHitWall = hit;
}
