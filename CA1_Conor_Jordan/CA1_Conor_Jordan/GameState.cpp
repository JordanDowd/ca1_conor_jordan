#include "GameState.hpp"
#include "MusicPlayer.hpp"

#include <SFML/Graphics/RenderWindow.hpp>


GameState::GameState(StateStack& stack, Context context)
: State(stack, context)
, mWorld(*context.window, *context.fonts, *context.sounds, *context.music,false)
, mPlayer1(nullptr, 1, context.keys1)
, mPlayer2(nullptr, 2, context.keys2)
{
	mWorld.addAircraft(1);
	mWorld.addAircraft(2);
	mPlayer1.setMissionStatus(Player::MissionRunning);
	mPlayer2.setMissionStatus(Player::MissionRunning);

	// Play game theme
	context.music->play(Music::MissionTheme);
}

void GameState::draw()
{
	mWorld.draw();
}

bool GameState::update(sf::Time dt)
{
	mWorld.update(dt);

	if (mWorld.mPlayer1Won)
	{
		mPlayer1.setMissionStatus(Player::MissionSuccess);
		requestStackPush(States::GreenTeamWins);
	}
	else if (mWorld.mPlayer2Won)
	{
		mPlayer2.setMissionStatus(Player::MissionSuccess);
		requestStackPush(States::RedTeamWins);
	}

	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer1.handleRealtimeInput(commands);
	mPlayer2.handleRealtimeInput(commands);

	return true;
}

bool GameState::handleEvent(const sf::Event& event)
{
	// Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer1.handleEvent(event, commands);
	mPlayer2.handleEvent(event, commands);

	// Escape pressed, trigger the pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
		requestStackPush(States::Pause);

	return true;
}